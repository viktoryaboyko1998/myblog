﻿using AutoMapper;
using BLL.DTO;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB.Mappers
{
	public class AutomapperProfile : Profile
	{
		public AutomapperProfile()
		{
			CreateMap<Article, ArticleDTO>().ReverseMap();
			CreateMap<Comment, CommentDTO>().ReverseMap();
			CreateMap<Blog, BlogDTO>().ReverseMap();
			CreateMap<UserProfile, UserDTO>().ReverseMap();
			CreateMap<Tag, TagDTO>().ReverseMap();
			CreateMap<ChangeData, ChangeDataDTO>().ReverseMap();
		}
	}
}