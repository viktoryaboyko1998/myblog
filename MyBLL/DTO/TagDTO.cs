﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BLL.DTO
{
	public class TagDTO
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public int ArticleId { get; set; }
	}
}
