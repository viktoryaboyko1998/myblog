﻿using System;
using System.Collections.Generic;

namespace BLL.DTO
{
    public class ArticleDTO
    {
		public int Id { get; set; }
		public string Title { get; set; }
		public string Text { get; set; }
		public bool IsDelete { get; set; }
		public bool IsAvatar { get; set; }
		public DateTime DateTime { get; set; }

		public string ProfileId { get; set; }
		public  UserDTO UserProfile { get; set; }
		public  BlogDTO Blog { get; set; }
		public int BlogId { get; set; }

		public ICollection<CommentDTO> Comments { get; set; }
		public ICollection<TagDTO> Tags { get; set; }

	}
}
