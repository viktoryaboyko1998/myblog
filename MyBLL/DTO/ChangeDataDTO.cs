﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
	public class ChangeDataDTO
	{
		public int Id { get; set; }
		public string Description { get; set; }
		public DateTime DateChange { get; set; }


		public string ProfileId { get; set; }
		public UserDTO UserProfile { get; set; }

		public int? ArticleId { get; set; }
		public ArticleDTO Article { get; set; }
		public int? CommentId { get; set; }
		public CommentDTO Comment { get; set; }
	}
}
