﻿using System;
using System.Collections.Generic;

namespace BLL.DTO
{
	public class CommentDTO
	{
		public int Id { get; set; }
		public string Text { get; set; }
		public DateTime DateTime { get; set; }
		public bool IsDelete { get; set; }
		//public bool IsChanged { get; set; }
		//public DateTime DateChange { get; set; }
		public  UserDTO UserProfile { get; set; }
		public  ArticleDTO Article { get; set; }
		public string ProfileId { get; set; }
		public int? ArticleId { get; set; }
		public int? CommentsReplyId { get; set; }
		public ICollection<CommentDTO> CommentsReply { get; set; }
	}
}
