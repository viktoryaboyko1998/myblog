﻿using System;
using System.Collections.Generic;
namespace BLL.DTO
{
	public class BlogDTO
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public DateTime DateTime { get; set; }
		public bool IsDelete { get; set; }
		public  ICollection<ArticleDTO> Articles { get; set; }
		public  UserDTO UserProfile { get; set; }
		public  string ProfileId { get; set; }
	}
}
