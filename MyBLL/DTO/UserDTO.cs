﻿using DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;

namespace BLL.DTO
{
    public class UserDTO
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        //public string UserName { get; set; }
        public string PasswordCheckCurrent { get; set; }
        //public string Role { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public DateTime BirthdayDate { get; set; }
        //public string RoleId { get; set; }
        public string RoleName { get; set; }
        public IList<string> UserRoles { get; set; }
        public List<IdentityRole> AllRoles { get; set; }
        //public List<ApplicationRole> AllRoles { get; set; }=new List<ApplicationRole>();
        public bool IsDelete { get; set; }

        public ICollection<BlogDTO> Blogs { get; set; }
        public ICollection<ArticleDTO> Articles { get; set; }
        public ICollection<CommentDTO> Comments { get; set; }
    }
}
