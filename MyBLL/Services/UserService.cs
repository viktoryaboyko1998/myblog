﻿using BLL.DTO;
using BLL.Infrastructure;
using DAL.Entities;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using BLL.Interfaces;
using DAL.Interfaces;
using System.Collections.Generic;
using AutoMapper;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }
        private readonly IMapper mapper;

        public UserService(IUnitOfWork uow, IMapper mapp)
        {
            Database = uow;
            mapper = mapp;
        }

        /// <summary>
        /// Add User in our system
        /// </summary>
        /// <param name="userDto">User`s data.</param>
        /// <returns> Operation Details.</returns>
        public async Task<OperationDetails> Create(UserDTO userDto)
        {
            ApplicationUser user = await Database.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {
                user = new ApplicationUser { Email = userDto.Email, UserName = userDto.Email };
                await Database.UserManager.CreateAsync(user, userDto.Password);
                // добавляем роль
                await Database.UserManager.AddToRoleAsync(user.Id, userDto.RoleName);
                // создаем профиль клиента
                UserProfile clientProfile = new UserProfile { Id = user.Id, BirthdayDate = userDto.BirthdayDate, Name = userDto.Name };
                Database.ClientManager.Create(clientProfile);
                await Database.SaveAsync();
                return new OperationDetails(true, "Registr is success", "");
            }
            else
            {
                return new OperationDetails(false, "User with this Email already exist", "Email");
            }
        }

        /// <summary>
        /// Authenticate User in our system
        /// </summary>
        /// <param name="userDto">User`s data.</param>
        /// <returns> claim</returns>
        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
        {
            ClaimsIdentity claim = null;
            // находим пользователя
            ApplicationUser user = await Database.UserManager.FindAsync(userDto.Email, userDto.Password);
            // авторизуем его и возвращаем объект ClaimsIdentity
            if (user != null)
                claim = await Database.UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
            return claim;
        }

        /// <summary>
        /// Get rolles in our system
        /// </summary>
        /// <param name="userDto">User`s data.</param>
        /// <returns> roles</returns>
        public List<IdentityRole> GetRoles()
        {
            return (List<IdentityRole>)Database.RoleManager.Roles;
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        /// <summary>
        /// Get All users Async
        /// </summary>
        /// <returns> list of users .</returns>
        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            var usersProfile = await Database.Profiles.GetAllAsync();

            List<UserDTO> usersDTO = new List<UserDTO>();

            foreach (var uP in usersProfile)
            {
                var userRoles = await Database.UserManager.GetRolesAsync(uP.Id);
                var comments = mapper.Map<ICollection<Comment>, ICollection<CommentDTO>>(uP.Comments);
                var articles = mapper.Map<ICollection<Article>, ICollection<ArticleDTO>>(uP.Articles);
                usersDTO.Add(new UserDTO
                {
                    Id = uP.Id,
                    Articles = articles,
                    Avatar = uP.Avatar,
                    BirthdayDate = uP.BirthdayDate,
                    Comments = comments,
                    //Blogs = uP.Blogs,
                    Email = uP.ApplicationUser.Email,
                    IsDelete = uP.IsDelete,
                    Name = uP.Name,
                    //Password = uP.ApplicationUser.PasswordHash,
                    UserRoles = userRoles,
                    RoleName = userRoles.First(),
                });
            }
            return usersDTO;
        }

        /// <summary>
        /// Gets user by id
        /// </summary>
        /// <param name="id">Id of user.</param>
        /// <returns>user`s data.</returns>
        public async Task<UserDTO> GetItemByIdAsync(string id)
        {

            var userProfile = await Database.Profiles.GetItemByIdAsync(id);
            var userRoles = await Database.UserManager.GetRolesAsync(userProfile.Id);
            var comments = mapper.Map<ICollection<Comment>, ICollection<CommentDTO>>(userProfile.Comments);
            var articles = mapper.Map<ICollection<Article>, ICollection<ArticleDTO>>(userProfile.Articles);

            return new UserDTO
            {
                Id = userProfile.Id,
                Articles = articles,
                Avatar = userProfile.Avatar,
                BirthdayDate = userProfile.BirthdayDate,
                Comments = comments,
                //Blogs = uP.Blogs,
                Email = userProfile.ApplicationUser.Email,
                IsDelete = userProfile.IsDelete,
                Name = userProfile.Name,
                //Password = userProfile.ApplicationUser.PasswordHash,
                UserRoles = userRoles,
                RoleName = userRoles.First(),
            };

        }

        /// <summary>
        /// Update user in our system
        /// </summary>
        /// <param name="item">user`s data.</param>
        public async Task<IEnumerable<string>> UpdateItemAsync(UserDTO item)
        {
            var user = mapper.Map<UserDTO, UserProfile>(item);
            //var oldUser = await Database.UserManager.FindByIdAsync(item.Id);
            IdentityResult result = new IdentityResult();
           /* if (!String.IsNullOrEmpty(item.PasswordCheckCurrent) || !String.IsNullOrEmpty(item.Password))
            {
                result = await Database.UserManager.ChangePasswordAsync(user.Id, item.PasswordCheckCurrent, item.Password);


                if (result.Succeeded == false)
                {
                    return result.Errors;

                }
            }*/
           // var userA = await Database.UserManager.FindByIdAsync(item.Id);
           // userA.UserName = item.Name;

            Database.Profiles.Update(user);
           // await Database.UserManager.UpdateAsync(userA);
            await Database.SaveAsync();


            return result.Errors;
        }

        /// <summary>
        /// Update user in our system with roles
        /// </summary>
        /// <param name="item">user`s data.</param>
        public async Task UpdateItemAsyncRoles(UserDTO item, List<string> roles)
        {
            var user = mapper.Map<UserDTO, UserProfile>(item);
            var userRoles = await Database.UserManager.GetRolesAsync(user.Id);
            // получаем все роли
            var allRoles = Database.RoleManager.Roles.ToList();
            // получаем список ролей, которые были добавлены
            var addedRoles = roles.Except(userRoles);
            string[] add = new string[addedRoles.Count()];
            int i = 0;
            foreach (var it in addedRoles)
            {
                add[i] = it;
                i++;
            }
            // получаем роли, которые были удалены
            var removedRoles = userRoles.Except(roles);
            string[] rem = new string[removedRoles.Count()];
            i = 0;
            foreach (var it in removedRoles)
            {
                rem[i] = it;
                i++;
            }


            await Database.UserManager.AddToRolesAsync(user.Id, add);
            await Database.UserManager.RemoveFromRolesAsync(user.Id, rem);
        }

        /// <summary>
        /// Update user`s pole in our system
        /// </summary>
        /// <param name="item">user`s data.</param>
        public async Task UpdateItemByAdminAsync(UserDTO item)
        {
            var user = mapper.Map<UserDTO, UserProfile>(item);
            Database.UserManager.ChangePassword(user.Id, item.PasswordCheckCurrent, item.Password);
            var userA = new ApplicationUser { Email = item.Email, UserName = item.Email };
            Database.UserManager.Update(userA);
            await Database.UserManager.RemoveFromRoleAsync(user.Id, "user");
            await Database.UserManager.AddToRoleAsync(user.Id, "manager");
            Database.Profiles.Update(user);
            //userM.Roles = "manager";
            await Database.SaveAsync();

        }
    }
}
