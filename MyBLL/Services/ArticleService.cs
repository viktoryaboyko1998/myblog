﻿using System;
using BLL.DTO;
using BLL.Interfaces;
using System.Collections.Generic;
using AutoMapper;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Threading.Tasks;
using WEB.Mappers;
using BLL.Infrastructure;

namespace BLL.Services
{
    public class ArticleService : IArticleService
    {
        IUnitOfWork Database { get; set; }
        private readonly IMapper mapper;

        public ArticleService(IUnitOfWork uow, IMapper mapp)
        {
            Database = uow;
            mapper = mapp;
        }

        /// <summary>
        /// Gets a list of all articles
        /// </summary>
        /// <returns>list of all articles.</returns>
        public async Task<IEnumerable<ArticleDTO>> GetAllAsync()
        {
            var date = await Database.Articles.GetAllAsync();
            return mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDTO>>(date);
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        /// <summary>
        /// Gets Article by id
        /// </summary>
        /// <param name="id">Id of article.</param>
        /// <returns>ArticleDTO`s data.</returns>
        public async Task<ArticleDTO> GetItemAsync(int? id)
        {
            var article = await Database.Articles.GetItemByIdAsync(id.ToString());
            if (article != null)
            {
                var blog = mapper.Map<Blog, BlogDTO>(await Database.Blogs.GetItemByIdAsync(article.BlogId.ToString()));
                var user = mapper.Map<UserProfile, UserDTO>(await Database.Profiles.GetItemByIdAsync(article.ProfileId.ToString()));
                var commentsDTO = mapper.Map<ICollection<Comment>, ICollection<CommentDTO>>(article.Comments);
                var tags = mapper.Map<ICollection<Tag>, ICollection<TagDTO>>(article.Tags);

                if (article.IsDelete)
                    return null;
                else
                    return new ArticleDTO
                    {
                        Title = article.Title,
                        DateTime = article.DateTime,
                        Text = article.Text,
                        Id = article.Id,
                        Blog = blog,
                        BlogId = article.BlogId,
                        UserProfile = user,
                        IsAvatar = article.IsAvatar,
                        IsDelete = article.IsDelete,
                        ProfileId = article.ProfileId,
                        Tags = tags,
                        Comments = commentsDTO
                    };
            }
            else
                return null;
        }

        /// <summary>
        /// Add Article in our system
        /// </summary>
        /// <param name="item">Article`s data.</param>
        public async Task AddItemAsync(ArticleDTO item)
        {
            var article = mapper.Map<ArticleDTO, Article>(item);
            Database.Articles.Create(article);
            await Database.SaveAsync();
        }

        /// <summary>
        /// Update Article in our system
        /// </summary>
        /// <param name="item">Article`s data.</param>
        public async Task UpdateItemAsync(ArticleDTO item)
        {

            var article = mapper.Map<ArticleDTO, Article>(item);
            var newTags = item.Tags;
            var result = await Database.Articles.GetItemByIdAsync(item.Id.ToString());
            var curentTags = result.Tags;
            var newCurentTags = result.Tags;
            var needDelete = true;
            var needAdd = true;
            Dictionary<string, Tag> tagsDelete = new Dictionary<string, Tag>();
            Dictionary<string, TagDTO> tagsAdd = new Dictionary<string, TagDTO>();

            foreach (var cTag in curentTags)
            {
                foreach (var nTag in newTags)
                {
                    if (cTag.Title == nTag.Title)
                        needDelete = false;
                }
                if (needDelete)
                {
                    tagsDelete.Add(cTag.Title, cTag);
                }
            }
            foreach (KeyValuePair<string, Tag> keyValue in tagsDelete)
            {
                await Database.Tags.RemoveFromArticleAsync(keyValue.Key, item.Id);
                newCurentTags.Remove(keyValue.Value);
            }

            foreach (var nTag in newTags)
            {
                foreach (var cTag in newCurentTags)
                {
                    if (cTag.Title == nTag.Title)
                        needAdd = false;
                }
                if (needAdd)
                {
                    tagsAdd.Add(nTag.Title, nTag);

                }
            }
            foreach (KeyValuePair<string, TagDTO> keyValue in tagsAdd)
            {
                await Database.Tags.AddToArticle(keyValue.Key, item.Id);
            }
            Database.Articles.Update(article);

            ChangeDataDTO changeDataDTO = new ChangeDataDTO();
            changeDataDTO.Article = item;
            changeDataDTO.ArticleId = item.Id;
            changeDataDTO.DateChange = DateTime.Now;
            Database.ChangeData.Create(mapper.Map<ChangeDataDTO, ChangeData>(changeDataDTO));
            await Database.SaveAsync();
        }


        /// <summary>
        /// Delate Article in our system
        /// </summary>
        /// <param name="id">Article`s id.</param>
        public async Task DeleteItemByIdAsync(int id)
        {
            Database.Articles.DeleteById(id);
            await Database.SaveAsync();
        }

        /// <summary>
        /// Gets a list of all blogs
        /// </summary>
        /// <returns> list of all blogs.</returns>
        public async Task<IEnumerable<BlogDTO>> GetAllBlogsAsync()
        {
            var date = await Database.Blogs.GetAllAsync();
            var blogs = date.Where(b => b.IsDelete == false);
            return mapper.Map<IEnumerable<Blog>, IEnumerable<BlogDTO>>(date);
        }

        /// <summary>
        /// Gets a list of all tags
        /// </summary>
        /// <returns> list of all tags.</returns>
        public async Task<IEnumerable<TagDTO>> GetAllTagsAsync()
        {
            var date = await Database.Tags.GetAllAsync();
            return mapper.Map<IEnumerable<Tag>, IEnumerable<TagDTO>>(date);
        }

        /// <summary>
        /// Gets a list of all comments
        /// </summary>
        /// <returns> list of all comments.</returns>
        public async Task<IEnumerable<CommentDTO>> GetAllCommentAsync()
        {
            var date = await Database.Comments.GetAllAsync();
            return mapper.Map<IEnumerable<Comment>, IEnumerable<CommentDTO>>(date);
        }

        /// <summary>
        /// Add Comment To Article Async
        /// </summary>
        /// <param name="comment">comment`s data.</param>
        public async Task AddCommentToArticleAsync(CommentDTO comment)
        {
            Database.Comments.Create(mapper.Map<Comment>(comment));
            await Database.SaveAsync();
        }

        /// <summary>
        /// Get All articles By User Id Async
        /// </summary>
        /// <param name="id">id user.</param>
        /// <returns> list of articles .</returns>
        public async Task<IEnumerable<ArticleDTO>> GetAllByUserIdAsync(string id)
        {
            var date = await Database.Articles.GetAllAsync();
            var dateByUser = date.Where(s => s.ProfileId == id && s.IsDelete==false);
            return mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDTO>>(dateByUser);
        }

        /// <summary>
        /// Get articles  with isDelete = false By User Id Async
        /// </summary>
        /// <param name="searchTitle">Specifies what text should be included in the title of Articles.</param>
        /// <param name="searchText">Specifies what text should be included in the text of articles</param>
        /// <param name="tags">Indicates which tag the article should have.</param>	
        /// <param name="sorting">Indicates on what basis to sort.</param>
        /// <returns> list of articles .</returns>
        public IEnumerable<ArticleDTO> GetAllFunction(IEnumerable<Article> result, string searchTitle, string searchText, string tags, Sort? sorting)
        {
            if ((searchTitle != null) && (searchTitle != " "))
                result = result.Where(n => n.Title.ToUpper().Contains(searchTitle.ToUpper()));
            else if ((searchText != null) && (searchText != " "))
                result = result.Where(n => n.Text.ToUpper().Contains(searchText.ToUpper()));
            else if ((tags != null) && (tags != " "))
                result = result.Where(n => n.Tags.Any(t => t.Title.ToUpper() == tags.ToUpper()));


            if (sorting != null)
            {
                switch (sorting)
                {
                    case Sort.DateAsc:
                        result = result.OrderBy(s => s.DateTime);
                        break;
                    case Sort.DateDesc:
                        result = result.OrderByDescending(s => s.DateTime);
                        break;
                    case Sort.TitleAsc:
                        result = result.OrderBy(s => s.Title);
                        break;
                    case Sort.TitleDesc:
                        result = result.OrderByDescending(s => s.Title);
                        break;
                }
            }
            else
            {
                result = result.OrderByDescending(s => s.DateTime);
            }

            return mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDTO>>(result);
        }

        /// <summary>
        /// Get All articles By User Id Async
        /// </summary>
        /// <param name="id">User1s id.</param>
        /// <param name="searchTitle">Specifies what text should be included in the title of Articles.</param>
        /// <param name="searchText">Specifies what text should be included in the text of articles</param>
        /// <param name="tags">Indicates which tag the article should have.</param>	
        /// <param name="sorting">Indicates on what basis to sort.</param>
        /// <returns> list of articles .</returns>
        public async Task<IEnumerable<ArticleDTO>> GetAllByUserIdAsync(string id, string searchTitle, string searchText, string tags, Sort? sorting)
        {

            var date = await Database.Articles.GetAllAsync();
            var dateByUser = date.Where(s => s.ProfileId == id && s.IsDelete==false);
            return GetAllFunction(dateByUser, searchTitle, searchText, tags, sorting);
        }

        /// <summary>
        /// Get articles  with isDelete = false By User Id Async
        /// </summary>
        /// <param name="id">User1s id.</param>
        /// <param name="searchTitle">Specifies what text should be included in the title of Articles.</param>
        /// <param name="searchText">Specifies what text should be included in the text of articles</param>
        /// <param name="tags">Indicates which tag the article should have.</param>	
        /// <param name="sorting">Indicates on what basis to sort.</param>
        /// <returns> list of articles .</returns>
        public async Task<IEnumerable<ArticleDTO>> GetAllByUserIdIsDeleteAsync(string id, string searchTitle, string searchText, string tags, Sort? sorting)
        {

            var date = await Database.Articles.GetAllAsync();
            var dateByUser = date.Where(s => s.ProfileId == id && s.IsDelete == false);
            return GetAllFunction(dateByUser, searchTitle, searchText, tags, sorting);
        }

        /// <summary>
        /// Get All articles Async
        /// </summary>
        /// <param name="searchTitle">Specifies what text should be included in the title of Articles.</param>
        /// <param name="searchText">Specifies what text should be included in the text of articles</param>
        /// <param name="tags">Indicates which tag the article should have.</param>	
        /// <param name="sorting">Indicates on what basis to sort.</param>
        /// <returns> list of articles .</returns>
        public async Task<IEnumerable<ArticleDTO>> GetAllIsDeleteAsync(string searchTitle, string searchText, string tags, Sort? sorting)
        {
            var date = await Database.Articles.GetAllAsync();
            var dateIsDelete = date.Where(a => a.IsDelete == false);
            return GetAllFunction(dateIsDelete, searchTitle, searchText, tags, sorting);
        }

        /// <summary>
        /// Get All articles Async
        /// </summary>
        /// <param name="searchTitle">Specifies what text should be included in the title of Articles.</param>
        /// <param name="searchText">Specifies what text should be included in the text of articles</param>
        /// <param name="tags">Indicates which tag the article should have.</param>	
        /// <param name="sorting">Indicates on what basis to sort.</param>
        /// <returns> list of articles .</returns>
        public async Task<IEnumerable<ArticleDTO>> GetAllAsync(string searchTitle, string searchText, string tags, Sort? sorting)
        {
            var date = await Database.Articles.GetAllAsync();
            var dateIsDelete = date.Where(a => a.IsDelete == false);
            return GetAllFunction(dateIsDelete, searchTitle, searchText, tags, sorting);
        }

        /// <summary>
        /// Set the article isDelete = true
        /// </summary>
        /// <param name="id">article id</param>
        public async Task DeleteItemByIIsDeletedAsync(int id)
        {
            Database.Articles.IsDeleteById(id);
            await Database.SaveAsync();
        }

        /// <summary>
        /// update comment`s data
        /// </summary>
        /// <param name="comment">comment`s data</param>
        public async Task UpdateCommentArticleAsync(CommentDTO comment)
        {
            Database.Comments.Update(mapper.Map<Comment>(comment));

            ChangeDataDTO changeDataDTO = new ChangeDataDTO();
            changeDataDTO.Comment = comment;
            changeDataDTO.CommentId = comment.Id;
            changeDataDTO.DateChange = DateTime.Now;
            Database.ChangeData.Create(mapper.Map<ChangeDataDTO, ChangeData>(changeDataDTO));
            await Database.SaveAsync();
        }

        /// <summary>
        /// delete comment
        /// </summary>
        /// <param name="id">comment`s id</param>
        public async Task DeleteCommentArticleAsync(int id)
        {
            Database.Comments.DeleteById(id);
            await Database.SaveAsync();
        }

        /// <summary>
        /// Get all Comment By Id Reply
        /// </summary>
        /// <param name="id">comment`s id</param>
        ///  <returns> list of comments .</returns>
        public async Task<CommentDTO> GetCommentByIdReply(int id)
        {
            var date = await Database.Comments.GetItemByIdAsync(id.ToString());
            return mapper.Map<Comment, CommentDTO>(date);
        }

        public async Task<ChangeDataDTO> GetLastChangeArticleByIdAsync(int id)
        {
            var date = Database.ChangeData.GetItemByIdArticleAsync(id);
           
                return mapper.Map<ChangeData, ChangeDataDTO>(date);
            
        }

        public async Task<ChangeDataDTO> GetLastChangeCommentByIdAsync(int id)
        {
            var date = await Database.ChangeData.GetItemByIdCommentAsync(id);
            return mapper.Map<ChangeData, ChangeDataDTO>(date);
        }
    }

}
