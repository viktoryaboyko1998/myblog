﻿using AutoMapper;
using BLL.Interfaces;
using DAL.Repositories;
using WEB.Mappers;

namespace BLL.Services
{
    public class ServiceCreator : IServiceCreator
    {
        public IUserService CreateUserService(string connection)
        {
            var mapperConfiguration = new MapperConfiguration(cfg => {  cfg.AddProfile<AutomapperProfile>(); });
            return new UserService(new UnitOfWork(connection), new Mapper(mapperConfiguration));
        }
    }
}
