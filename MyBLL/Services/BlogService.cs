﻿using System;
using BLL.DTO;
using BLL.Interfaces;
using System.Collections.Generic;
using AutoMapper;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Threading.Tasks;
using WEB.Mappers;
using BLL.Infrastructure;

namespace BLL.Services
{
    public class BlogService : IService<BlogDTO>
    {
        IUnitOfWork Database { get; set; }
        private readonly IMapper mapper;

        public BlogService(IUnitOfWork uow, IMapper mapp)
        {
            Database = uow;
            mapper = mapp;
        }

        /// <summary>
        /// Gets a list of all blogs
        /// </summary>
        /// <returns>list of all articles.</returns>
        public async Task<IEnumerable<BlogDTO>> GetAllAsync()
        {
            var date = await Database.Blogs.GetAllAsync();
            return mapper.Map<IEnumerable<Blog>, IEnumerable<BlogDTO>>(date);
        }
        public void Dispose()
        {
            Database.Dispose();
        }

        /// <summary>
        /// Add Blog in our system
        /// </summary>
        /// <param name="item">Blog`s data.</param>
        public async Task AddItemAsync(BlogDTO item)
        {
            var blog = mapper.Map<BlogDTO, Blog>(item);
            Database.Blogs.Create(blog);
            await Database.SaveAsync();
        }

        /// <summary>
        /// Update Blog in our system
        /// </summary>
        /// <param name="item">Blog`s data.</param>
        public async Task UpdateItemAsync(BlogDTO item)
        {
            var blog = mapper.Map<BlogDTO, Blog>(item);
            Database.Blogs.Update(blog);
            await Database.SaveAsync();
        }

        /// <summary>
        /// Delate Blog in our system
        /// </summary>
        /// <param name="id">Blog`s id.</param>
        public async Task DeleteItemByIdAsync(int id)
        {
            Database.Blogs.DeleteById(id);
            await Database.SaveAsync();
        }

        public async Task<BlogDTO> GetItemAsync(int? id)
        {
            var blog = await Database.Blogs.GetItemByIdAsync(id.ToString());
            var user = mapper.Map<UserProfile, UserDTO>(await Database.Profiles.GetItemByIdAsync(blog.ProfileId.ToString()));
            var article = mapper.Map<ICollection<Article>, ICollection<ArticleDTO>>(blog.Articles);
            if (blog.IsDelete)
                return null;
            else
                return new BlogDTO { Title = blog.Title, DateTime = blog.DateTime, Id = blog.Id, ProfileId = blog.ProfileId, Articles=article, UserProfile=user, IsDelete= blog.IsDelete };
        }

        /// <summary>
        /// Get All blog By User Id Async
        /// </summary>
        /// <param name="id">id user.</param>
        /// <returns> list of blogs .</returns>
        public async Task<IEnumerable<BlogDTO>> GetAllByUserIdAsync(string id)
        {
            var date = await Database.Blogs.GetAllAsync();
            var dateByUser = date.Where(s => s.ProfileId == id && s.IsDelete==false);
            return mapper.Map<IEnumerable<Blog>, IEnumerable<BlogDTO>>(dateByUser);
        }

        /// <summary>
        /// Get blogs By User Id Async
        /// </summary>
        /// <param name="searchTitle">Specifies what text should be included in the title of Articles.</param>
        /// <param name="searchText">Specifies what text should be included in the text of articles</param>
        /// <param name="tags">Indicates which tag the article should have.</param>	
        /// <param name="sorting">Indicates on what basis to sort.</param>
        /// <returns> list of articles .</returns>
        public IEnumerable<BlogDTO> GetAllFunction(IEnumerable<Blog> date, string searchTitle, string searchText, string tags, Sort? sorting)
        {
            IEnumerable<Blog> result;
            if (searchTitle != null)
                result = date.Where(n => n.Title.ToUpper().Contains(searchTitle.ToUpper()));
            else
                result = date;


            if (sorting != null)
            {
                switch (sorting)
                {
                    case Sort.DateAsc:
                        result = result.OrderBy(s => s.DateTime);
                        break;
                    case Sort.DateDesc:
                        result = result.OrderByDescending(s => s.DateTime);
                        break;
                    case Sort.TitleAsc:
                        result = result.OrderBy(s => s.Title);
                        break;
                    case Sort.TitleDesc:
                        result = result.OrderByDescending(s => s.Title);
                        break;
                }
            }
            else
            {
                result = result.OrderBy(s => s.DateTime);
            }

            return mapper.Map<IEnumerable<Blog>, IEnumerable<BlogDTO>>(result);
        }

        /// <summary>
        /// Get All blogs By User Id Async
        /// </summary>
        /// <param name="id">User1s id.</param>
        /// <param name="searchTitle">Specifies what text should be included in the title of Articles.</param>
        /// <param name="searchText">Specifies what text should be included in the text of articles</param>
        /// <param name="tags">Indicates which tag the article should have.</param>	
        /// <param name="sorting">Indicates on what basis to sort.</param>
        /// <returns> list of blogs .</returns>
        public async Task<IEnumerable<BlogDTO>> GetAllByUserIdAsync(string id, string searchTitle, string searchText, string tags, Sort? sorting)
        {
            var date = await Database.Blogs.GetAllAsync();
            var dateByUser = date.Where(s => s.ProfileId == id && s.IsDelete==false);
            return GetAllFunction(dateByUser, searchTitle, searchText, tags, sorting);

        }

        /// <summary>
        /// Get All blogs Async
        /// </summary>
        /// <param name="searchTitle">Specifies what text should be included in the title of Articles.</param>
        /// <param name="searchText">Specifies what text should be included in the text of articles</param>
        /// <param name="tags">Indicates which tag the article should have.</param>	
        /// <param name="sorting">Indicates on what basis to sort.</param>
        /// <returns> list of blogs .</returns>
        public async Task<IEnumerable<BlogDTO>> GetAllAsync(string searchTitle, string searchText, string tags, Sort? sorting)
        {
            var date = await Database.Blogs.GetAllAsync();
            var blogs = date.Where(b => b.IsDelete == false);
            return GetAllFunction(blogs, searchTitle, searchText, tags, sorting);
        }

        /// <summary>
        /// set the blog isDelete= true
        /// </summary>
        /// <param name="id">blog id</param>
        public async Task<IEnumerable<BlogDTO>> GetAllIsDeleteAsync(string searchTitle, string searchText, string tags, Sort? sorting)
        {
            var date = await Database.Blogs.GetAllAsync();
            var dateIsDel = date.Where(b => b.IsDelete == false);

            return GetAllFunction(dateIsDel, searchTitle, searchText, tags, sorting);
        }

        /// <summary>
        /// Get blogs  with isDelete = false By User Id Async
        /// </summary>
        /// <param name="id">User1s id.</param>
        /// <param name="searchTitle">Specifies what text should be included in the title of Articles.</param>
        /// <param name="searchText">Specifies what text should be included in the text of articles</param>
        /// <param name="tags">Indicates which tag the article should have.</param>	
        /// <param name="sorting">Indicates on what basis to sort.</param>
        /// <returns> list of blogs .</returns>
        public async Task<IEnumerable<BlogDTO>> GetAllByUserIdIsDeleteAsync(string id, string searchTitle, string searchText, string tags, Sort? sorting)
        {
            var date = await Database.Blogs.GetAllAsync();
            var dateByUser = date.Where(s => s.ProfileId == id && s.IsDelete == false);
            return GetAllFunction(dateByUser, searchTitle, searchText, tags, sorting);
        }

        /// <summary>
        /// Set the blog and its articles isDelete = true
        /// </summary>
        /// <param name="id">blog id</param>
        public async Task DeleteItemByIIsDeletedAsync(int id)
        {
            Database.Blogs.IsDeleteById(id);
            var articles = await Database.Articles.GetAllAsync();
            foreach(var article in articles)
            {
                if(article.BlogId==id)
                {
                    article.IsDelete = true;
                    Database.Articles.Update(article);
                }
            }
            await Database.SaveAsync();
        }
    }
}
