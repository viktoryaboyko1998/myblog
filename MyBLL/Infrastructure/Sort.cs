﻿namespace BLL.Infrastructure
{
	public enum Sort
	{
		DateAsc,
		DateDesc,
		TitleAsc,
		TitleDesc
	}
	
}
