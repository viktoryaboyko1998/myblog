﻿using Ninject.Modules;
using DAL.Interfaces;
using DAL.Repositories;
using AutoMapper;
using BLL.Interfaces;
using BLL.Services;
using WEB.Mappers;

namespace BLL.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        private string connectionString;
        public ServiceModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            
            //Bind<IService>().To<ArticleService>().InSingletonScope();
           // Bind<IService>().To<TagService>().InSingletonScope();
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile<AutomapperProfile>(); });
            //Bind<IMapper>().ToConstructor(s => new Mapper(mapperConfiguration));
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(connectionString);

        }
    }
}