﻿using BLL.DTO;
using BLL.Infrastructure;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IService<T>
    {
        Task<T> GetItemAsync(int? id);
        //Task<IEnumerable<T>> GetAllAsync();
        Task<IEnumerable<T>> GetAllAsync(string searchTitle, string searchText, string tags, Sort? sorting);
        Task<IEnumerable<T>> GetAllIsDeleteAsync(string searchTitle, string searchText, string tags, Sort? sorting);
        Task AddItemAsync(T item);
        Task UpdateItemAsync(T item);
        Task DeleteItemByIdAsync(int id);
        Task DeleteItemByIIsDeletedAsync(int id);
        //Task<IEnumerable<T>> GetAllByUserIdAsync(string id);
        Task<IEnumerable<T>> GetAllByUserIdAsync(string id, string searchTitle, string searchText, string tags, Sort? sorting);
        Task<IEnumerable<T>> GetAllByUserIdIsDeleteAsync(string id, string searchTitle, string searchText, string tags, Sort? sorting);
    }
}
