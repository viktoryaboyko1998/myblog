﻿using BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IArticleService : IService<ArticleDTO>
    {
        Task<IEnumerable<BlogDTO>> GetAllBlogsAsync();
        Task<IEnumerable<TagDTO>> GetAllTagsAsync();
        Task<IEnumerable<CommentDTO>> GetAllCommentAsync();
        Task<ChangeDataDTO> GetLastChangeArticleByIdAsync(int id);
        Task<ChangeDataDTO> GetLastChangeCommentByIdAsync(int id);
        Task<CommentDTO> GetCommentByIdReply(int id);
        Task AddCommentToArticleAsync(CommentDTO comment);
        Task UpdateCommentArticleAsync(CommentDTO comment);
        Task DeleteCommentArticleAsync(int id);
    }
}
