﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Infrastructure;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        Task<OperationDetails> Create(UserDTO userDto);
        Task<ClaimsIdentity> Authenticate(UserDTO userDto);
        //Task SetInitialData(UserDTO adminDto, List<string> roles);


        //Dictionary<string, string> GetRoles();
        List<IdentityRole> GetRoles();





        Task<IEnumerable<UserDTO>> GetAllAsync();
        Task<UserDTO> GetItemByIdAsync(string id);
        Task UpdateItemAsyncRoles(UserDTO item, List<string> roles);
        Task<IEnumerable<string>> UpdateItemAsync(UserDTO item);
        Task UpdateItemByAdminAsync(UserDTO item );

    } 
}
