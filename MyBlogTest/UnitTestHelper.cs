﻿using AutoMapper;
using DAL.EF;
using DAL.Entities;
using DAL.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Text;
using WEB.Mappers;

namespace MyBlogTest
{
    internal static class UnitTestHelper
    {
        public static void GetUnitTestDbOptions()
        {
            using (var context = new ApplicationContext("MyBlogConnection"))
            {
                SeedData(context);
            }
        }

        public static void SeedData(ApplicationContext context)
        {
			var roles = new ApplicationRoleManager(new RoleStore<ApplicationRole>(context));
			roles.Create(new ApplicationRole() { Name = "user" });
			roles.Create(new ApplicationRole() { Name = "admin" });
			roles.Create(new ApplicationRole() { Name = "manager" });

			var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context), new DpapiDataProtectionProvider());
			var user = new ApplicationUser { Email = "viktoriiaboiko1998@gmail.com", UserName = "Viktoriia" };
			manager.Create(user, "111111");
			manager.AddToRole(user.Id, "admin");
			var user2 = new ApplicationUser { Email = "user@gmail.com", UserName = "Helen" };
			var result = manager.Create(user2, "111111");
			if (result.Succeeded)
			{
				//add this to add role to user
				manager.AddToRole(user2.Id, "user");
			}

			var newUser2 = new UserProfile
			{
				Name = "Helen Dudnichenko",
				BirthdayDate = new DateTime(1994, 3, 08),
				Id = user2.Id,
				Avatar = ""
			};
			context.UserProfile.Add(newUser2);

			var newUser = new UserProfile
			{
				Name = "Viktoriia Boiko",
				BirthdayDate = new DateTime(2008, 3, 15),
				Id = user.Id,
				Avatar = ""
			};
			context.UserProfile.Add(newUser);
			var blog = new Blog()
			{
				ProfileId = newUser.Id,
				//UserProfile = newUser,
				Title = "Books",
				DateTime = DateTime.Now
			};
			context.Blogs.Add(blog);
			var article = new Article
			{
				Title = "White fang",
				Text = $"White Fang is a novel by American author Jack London (1876–1916) — and the name of the book's eponymous character," +
				$"a wild wolfdog. First serialized in Outing magazine, it was published in 1906. The story details White Fang's journey to " +
				$"domestication in Yukon Territory and the Northwest Territories during the 1890s Klondike Gold Rush. It is a companion novel " +
				$"(and a thematic mirror) to London's best-known work, The Call of the Wild (1903), which is about a kidnapped, domesticated dog" +
				$" embracing his wild ancestry to survive and thrive in the wild." +
				$"Much of White Fang is written from the viewpoint of the titular canine character, enabling London to explore how animals view " +
				$"their world and how they view humans.White Fang examines the violent world of wild animals and the equally violent world of" +
				$" humans.The book also explores complex themes including morality and redemption." +
				$"As early as 1925, the story was adapted to film, and it has since seen several more cinematic adaptations, including a 1991 " +
				$"film starring Ethan Hawke and a 2018 original film for Netflix.",
				//UserProfile = newUser,
				DateTime = DateTime.Now,
				ProfileId = newUser.Id,
				BlogId = blog.Id,
				//Blog = blog
			};
			var tag = new Tag
			{
				Title = "2020"
			};
			var tag1 = new Tag
			{
				Title = "Adventure"
			};
			var tag2 = new Tag
			{
				Title = "Programming"
			};
			var listTag = new List<Tag>();
			listTag.Add(tag);
			listTag.Add(tag1);
			context.Tags.AddRange(listTag);


			var comment = new Comment()
			{
				ArticleId = article.Id,
				//Article = article,
				//UserProfile = newUser,
				ProfileId = newUser.Id,
				DateTime = DateTime.Now,
				Text = "It is my love book!!!"
			};
			var comment1 = new Comment()
			{
				ArticleId = article.Id,
				//Article = article,
				//UserProfile = newUser,
				ProfileId = newUser.Id,
				DateTime = DateTime.Now,
				Text = "The famous work 'White Fang', by Jack London, author of such works as 'Thirst for Life', 'Martin Eden' and 'Hearts of Three'," +
				$" is one of the most famous and popular with the author. The work mixed all the main themes of the author (except, perhaps -" +
				$" the marine theme), such as - gold mining, the opposition of man to nature, snowdrifts, animal life, as well as - the " +
				$"relationship between man-animals-nature. The work is about a half-wolf nicknamed White Fang. The wolf was left alone with" +
				$" his mother early on and was forced to grow up in his early years - his father died in a battle with a lynx. Driven by thirst," +
				$" White Fang went to the river to drink and bring water to his mother, who was a half-wolf and in poor health. At the river, " +
				$"the wolf met an unprecedented creature - a man. And here begins the adventures of a half-wolf in the human world - it turns out" +
				$" that this is the former owner of his mother, as evidenced by a strange feeling inside the White Fang. He will face severe trials " +
				$"that will bring him both joy and pain."
			};


			var comment2 = new Comment()
			{
				ArticleId = article.Id,
				Article = article,
				UserProfile = newUser2,
				ProfileId = newUser2.Id,
				DateTime = DateTime.Now,
				Text = "I don`t like this book"
			};

			var listComments = new List<Comment>();
			listComments.Add(comment);
			listComments.Add(comment1);
			listComments.Add(comment2);

			context.Comments.AddRange(listComments);
			article.Comments = listComments;
			article.Tags = listTag;

			context.Articles.Add(article);

			var blog2 = new Blog()
			{
				Id = 1,
				ProfileId = newUser.Id,
				UserProfile = newUser,
				Title = "IT",
				DateTime = DateTime.Now
			};
			context.Blogs.Add(blog2);
			var article2 = new Article
			{
				Title = "WHY DOES MACHINE TRAINING CHOOSE PYTHON?",
				Text = $"Maybe someone hasn't noticed this yet, but the mention of Python can be traced in conversations with colleagues and " +
				$"teachers, in teaching articles and videos, and, ultimately, in interview requirements. What has Python done with the №1 " +
				$"programming language for machine learning?" +
				$"WHAT IS 'MACHINE TRAINING' ?" +
				$"Technology called 'machine learning' is a subdivision of artificial intelligence, at least that's what Wikipedia says. So, " +
				$"this subsection deals with methods of building algorithms that are able to train computers and devices for independent, so " +
				$"to speak, decision-making." +
				$"WHAT DO WE KNOW ABOUT PYTHON ?" +
				$"- Python was born in a scientific environment. Guido van Rossum conceived Python in the 1980s, and began its creation in December" +
				$" 1989 at the Center for Mathematics and Computer Science in the Netherlands.From its inception, the language was created and " +
				$"used by scientists, becoming popular among them." +
				$"- Python is considered one of the simplest programming languages. Very simple, transparent and 'human-readable syntax'.And it " +
				$"is also characterized by simplicity in understanding the processes.You don't have to worry about compilation or other " +
				$"software development processes." +
				$"- Global community support. Over a period of time, a huge number of libraries have been created for Python, including machine" +
				$" learning.Third - party developers and the community around the world continue to add something new to the language. Of " +
				$"the most famous libraries for machine learning: Numpy, SciPy, SciKit - Learn.And various frameworks: TensorFlow, CNTK and" +
				$" Apache Spark.There is also a framework for Python designed specifically for machine learning -this is PyTorch." +
				$"- The amount of training information. Any beginner who will google the solution of a problem in the field of machine learning, " +
				$"will definitely come across a solution using the Python language.Manuals, videos, courses, etc.This information is the easiest" +
				$" to find, because it is popular.Therefore, no one is looking for a solution using other programming languages.This information " +
				$"is simply not easy to find." +
				$"IF THERE ARE ALTERNATIVES FOR PYTHON?" +
				$"R" +
				$"Students are introduced to this programming language at university, and I am no exception. Never heard of this language," +
				$" although it appeared in 1993, was developed by the staff of the Faculty of Statistics, University of Auckland. But despite the" +
				$" fact that this language is actively used in statistical calculations, it is quite rich in machine learning libraries." +
				$"I want to note that R has a rather unusual syntax even for experienced programmers. But it is important to understand that " +
				$"Python is more used in commercial projects.But R is very popular in scientific circles." +
				$"ML.NET Library for machine learning on the.NET platform, open and cross - platform.According to Microsoft, ML is now an" +
				$" alternative to Python tools, but for the.NET environment. It has a large enough functionality for machine learning.It is " +
				$"unknown at this time what he will do after leaving the post.But for ready - made enterprise applications built on.Net is a good " +
				$"option for implementing machine learning business solutions without using Python and its libraries." +
				$"But still, Python with its tools has long held the position of the most popular programming language for machine learning and" +
				$" the whole field of artificial intelligence, and I do not think it is going to give anyone its place." +
				$"White Fang is a novel by American author Jack London (1876–1916) — and the name of the book's eponymous character," +
				$"a wild wolfdog. First serialized in Outing magazine, it was published in 1906. The story details White Fang's journey to " +
				$"domestication in Yukon Territory and the Northwest Territories during the 1890s Klondike Gold Rush. It is a companion novel " +
				$"(and a thematic mirror) to London's best-known work, The Call of the Wild (1903), which is about a kidnapped, domesticated dog" +
				$" embracing his wild ancestry to survive and thrive in the wild." +
				$"Much of White Fang is written from the viewpoint of the titular canine character, enabling London to explore how animals view " +
				$"their world and how they view humans.White Fang examines the violent world of wild animals and the equally violent world of" +
				$" humans.The book also explores complex themes including morality and redemption." +
				$"As early as 1925, the story was adapted to film, and it has since seen several more cinematic adaptations, including a 1991 " +
				$"film starring Ethan Hawke and a 2018 original film for Netflix.",
				UserProfile = newUser,
				DateTime = DateTime.Now,
				ProfileId = newUser.Id,
				BlogId = blog2.Id,
				Blog = blog2
			};


			var listTag2 = new List<Tag>();
			listTag2.Add(tag2);
			article2.Tags = listTag2;

			context.Articles.Add(article2);
		}

        public static Mapper CreateMapperProfile()
        {
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }
    }
}
