﻿using BLL.DTO;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using WEB.Models;

namespace MyBlogTest
{
    internal class ArticleEqualityComparer : IEqualityComparer<Article>
    {
        public bool Equals([AllowNull] Article x, [AllowNull] Article y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.Title == y.Title
                && x.ProfileId == y.ProfileId
                && x.DateTime == y.DateTime
                && x.Tags == y.Tags
                && x.BlogId == y.BlogId
                && x.Comments == y.Comments
                && x.Text == y.Text;
        }

        public int GetHashCode([DisallowNull] Article obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class ArticleModelEqualityComparer : IEqualityComparer<ArticleModel>
    {
        public bool Equals([AllowNull] ArticleModel x, [AllowNull] ArticleModel y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.Title == y.Title
                && x.ProfileId == y.ProfileId
                && x.DateTime == y.DateTime
                && x.Tags == y.Tags
                && x.BlogId == y.BlogId
                && x.Comments == y.Comments
                && x.Text == y.Text;
        }

        public int GetHashCode([DisallowNull] ArticleModel obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class ArticleDTOEqualityComparer : IEqualityComparer<ArticleDTO>
    {
        public bool Equals([AllowNull] ArticleDTO x, [AllowNull] ArticleDTO y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.Title == y.Title
                && x.ProfileId == y.ProfileId
                && x.DateTime == y.DateTime
                && x.Tags == y.Tags
                && x.BlogId == y.BlogId
                && x.Comments == y.Comments
                && x.Text == y.Text;
        }

        public int GetHashCode([DisallowNull] ArticleDTO obj)
        {
            return obj.GetHashCode();
        }
    }
}
