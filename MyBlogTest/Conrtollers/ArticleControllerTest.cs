﻿using BLL.DTO;
using BLL.Interfaces;
using BLL.Services;
using DAL.Entities;
using DAL.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WEB.Controllers;
using WEB.Models;

namespace MyBlogTest.Conrtollers
{
    
    public class ArticleControllerTest
    {


        [Test]
        public async Task ArticleService_GetAll_ReturnsArticleModels()
        {
            var expected = GetTestArticleDTO();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.Articles.GetAllAsync().Result)
                .Returns(GetTestArticleEntities());
            IArticleService articleService = new ArticleService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var actual = await articleService.GetAllAsync(null,null,null,null);
            int i = 0;
            foreach(var item in actual)
            {
                Assert.AreEqual(expected[i].Id, item.Id);
                Assert.AreEqual(expected[i].DateTime, item.DateTime);
                Assert.AreEqual(expected[i].Text, item.Text);
                Assert.AreEqual(expected[i].Title, item.Title);
                Assert.AreEqual(expected[i].ProfileId, item.ProfileId);
                i++;
            }
        }

        private List<ArticleDTO> GetTestArticleDTO()
        {
            return new List<ArticleDTO>()
            {
                new ArticleDTO {Id = 1, Title = "Title1", Text = "Text1", ProfileId = "1"},
                new ArticleDTO {Id = 2, Title = "Title2", Text = "Text2", ProfileId = "1"},
                new ArticleDTO {Id = 3, Title = "Title3", Text = "Text3", ProfileId = "2"},
                new ArticleDTO {Id = 4, Title = "Title4", Text = "Text4", ProfileId = "2"}
            };
        }

        private List<Article> GetTestArticleEntities()
        {
            return new List<Article>()
            {
                new Article {Id = 1, Title = "Title1", Text = "Text1", ProfileId = "1"},
                new Article {Id = 2, Title = "Title2", Text = "Text2", ProfileId = "1"},
                new Article {Id = 3, Title = "Title3", Text = "Text3", ProfileId = "2"},
                new Article {Id = 4, Title = "Title4", Text = "Text4", ProfileId = "2"}
            };
        }
    }
}
