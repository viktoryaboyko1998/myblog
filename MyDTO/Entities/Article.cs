﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DAL.Entities;

namespace DAL.Entities
{
    public class Article
    {
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
		public string Title { get; set; }
		public string Text { get; set; }
		public bool IsDelete { get; set; }
		public bool  IsAvatar { get; set; }
		public DateTime DateTime { get; set; }


		[ForeignKey("UserProfile")]
		public string ProfileId { get; set; }
		public  UserProfile UserProfile { get; set; }
		[ForeignKey("Blog")]
		public int BlogId { get; set; }
		public  Blog Blog { get; set; }

		public  ICollection<Comment> Comments { get; set; }
		public  ICollection<Tag> Tags { get; set; }
	}
}
