﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
	public class Comment
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
		public string Text { get; set; }
		public DateTime DateTime { get; set; }
		public bool IsDelete { get; set; }
		//public bool IsChange { get; set; }
		//public DateTime DataChange { get; set; }


		[ForeignKey("UserProfile")]
		public string ProfileId { get; set; }
		public  UserProfile UserProfile { get; set; }
		[ForeignKey("Article")]
		public int? ArticleId { get; set; }
		public Article Article { get; set; }

		[ForeignKey("CommentsReply")]
		public int? CommentsReplyId { get; set; }
		public ICollection<Comment> CommentsReply { get; set; }
	}
}
