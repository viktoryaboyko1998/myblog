﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class CommentAll
    {
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
		public string Text { get; set; }
		public DateTime DateTime { get; set; }
		public bool IsDelete { get; set; }


		[ForeignKey("UserProfile")]
		public string ProfileId { get; set; }
		public UserProfile UserProfile { get; set; }
		[ForeignKey("Article")]
		public int? ArticleId { get; set; }
		public Article Article { get; set; }

		[ForeignKey("Comment")]
		public int? CommentId { get; set; }
		public Comment Comment { get; set; }
	}
}
