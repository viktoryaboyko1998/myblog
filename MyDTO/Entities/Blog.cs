﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DAL.Entities;

namespace DAL.Entities
{
	public class Blog
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		public string Title { get; set; }
		public DateTime DateTime { get; set; }
		public bool IsDelete { get; set; }


		public  ICollection<Article> Articles { get; set; }
		[ForeignKey("UserProfile")]
		public string ProfileId { get; set; }
		public  UserProfile UserProfile { get; set; }
	}
}
