﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.AspNet.Identity.Owin;
using DAL.Entities;

namespace DAL.Identity
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
                : base(store)
        {
        }

		public ApplicationUserManager(IUserStore<ApplicationUser> store, IDataProtectionProvider dataProtectionProvider)
				: base(store)
		{
			this.UserValidator = new UserValidator<ApplicationUser>(this)
			{
				AllowOnlyAlphanumericUserNames = true,
				RequireUniqueEmail = true
			};
			if (dataProtectionProvider != null)
			{
				IDataProtector dataProtector = dataProtectionProvider.Create("ASP.NET Identity");
				this.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtector);
			}
		}
	}
}
