namespace MyDTO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCommentAll : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommentAlls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        DateTime = c.DateTime(nullable: false),
                        IsDelete = c.Boolean(nullable: false),
                        ProfileId = c.String(maxLength: 128),
                        ArticleId = c.Int(),
                        CommentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articles", t => t.ArticleId)
                .ForeignKey("dbo.Comments", t => t.CommentId)
                .ForeignKey("dbo.UserProfiles", t => t.ProfileId)
                .Index(t => t.ProfileId)
                .Index(t => t.ArticleId)
                .Index(t => t.CommentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CommentAlls", "ProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.CommentAlls", "CommentId", "dbo.Comments");
            DropForeignKey("dbo.CommentAlls", "ArticleId", "dbo.Articles");
            DropIndex("dbo.CommentAlls", new[] { "CommentId" });
            DropIndex("dbo.CommentAlls", new[] { "ArticleId" });
            DropIndex("dbo.CommentAlls", new[] { "ProfileId" });
            DropTable("dbo.CommentAlls");
        }
    }
}
