namespace MyDTO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveChangeCommentAndChangeArticle : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ChangeArticles", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.ChangeArticles", "ProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.ChangeComments", "CommentId", "dbo.Comments");
            DropForeignKey("dbo.ChangeComments", "ProfileId", "dbo.UserProfiles");
            DropIndex("dbo.ChangeArticles", new[] { "ProfileId" });
            DropIndex("dbo.ChangeArticles", new[] { "ArticleId" });
            DropIndex("dbo.ChangeComments", new[] { "ProfileId" });
            DropIndex("dbo.ChangeComments", new[] { "CommentId" });
            DropTable("dbo.ChangeArticles");
            DropTable("dbo.ChangeComments");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ChangeComments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        DateChange = c.DateTime(nullable: false),
                        ProfileId = c.String(maxLength: 128),
                        CommentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ChangeArticles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        DateChange = c.DateTime(nullable: false),
                        ProfileId = c.String(maxLength: 128),
                        ArticleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.ChangeComments", "CommentId");
            CreateIndex("dbo.ChangeComments", "ProfileId");
            CreateIndex("dbo.ChangeArticles", "ArticleId");
            CreateIndex("dbo.ChangeArticles", "ProfileId");
            AddForeignKey("dbo.ChangeComments", "ProfileId", "dbo.UserProfiles", "Id");
            AddForeignKey("dbo.ChangeComments", "CommentId", "dbo.Comments", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ChangeArticles", "ProfileId", "dbo.UserProfiles", "Id");
            AddForeignKey("dbo.ChangeArticles", "ArticleId", "dbo.Articles", "Id", cascadeDelete: true);
        }
    }
}
