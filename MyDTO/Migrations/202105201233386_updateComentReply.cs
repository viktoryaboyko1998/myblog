namespace MyDTO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateComentReply : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Comments", name: "CommentId", newName: "CommentsReplyId");
            RenameIndex(table: "dbo.Comments", name: "IX_CommentId", newName: "IX_CommentsReplyId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Comments", name: "IX_CommentsReplyId", newName: "IX_CommentId");
            RenameColumn(table: "dbo.Comments", name: "CommentsReplyId", newName: "CommentId");
        }
    }
}
