namespace MyDTO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DellCommentAll : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CommentAlls", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.CommentAlls", "CommentId", "dbo.Comments");
            DropForeignKey("dbo.CommentAlls", "ProfileId", "dbo.UserProfiles");
            DropIndex("dbo.CommentAlls", new[] { "ProfileId" });
            DropIndex("dbo.CommentAlls", new[] { "ArticleId" });
            DropIndex("dbo.CommentAlls", new[] { "CommentId" });
            DropTable("dbo.CommentAlls");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CommentAlls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        DateTime = c.DateTime(nullable: false),
                        IsDelete = c.Boolean(nullable: false),
                        ProfileId = c.String(maxLength: 128),
                        ArticleId = c.Int(),
                        CommentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.CommentAlls", "CommentId");
            CreateIndex("dbo.CommentAlls", "ArticleId");
            CreateIndex("dbo.CommentAlls", "ProfileId");
            AddForeignKey("dbo.CommentAlls", "ProfileId", "dbo.UserProfiles", "Id");
            AddForeignKey("dbo.CommentAlls", "CommentId", "dbo.Comments", "Id");
            AddForeignKey("dbo.CommentAlls", "ArticleId", "dbo.Articles", "Id");
        }
    }
}
