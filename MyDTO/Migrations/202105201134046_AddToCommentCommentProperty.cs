namespace MyDTO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddToCommentCommentProperty : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "ArticleId", "dbo.Articles");
            DropIndex("dbo.Comments", new[] { "ArticleId" });
            AddColumn("dbo.Comments", "CommentId", c => c.Int());
            AlterColumn("dbo.Comments", "ArticleId", c => c.Int());
            CreateIndex("dbo.Comments", "ArticleId");
            CreateIndex("dbo.Comments", "CommentId");
            AddForeignKey("dbo.Comments", "CommentId", "dbo.Comments", "Id");
            AddForeignKey("dbo.Comments", "ArticleId", "dbo.Articles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.Comments", "CommentId", "dbo.Comments");
            DropIndex("dbo.Comments", new[] { "CommentId" });
            DropIndex("dbo.Comments", new[] { "ArticleId" });
            AlterColumn("dbo.Comments", "ArticleId", c => c.Int(nullable: false));
            DropColumn("dbo.Comments", "CommentId");
            CreateIndex("dbo.Comments", "ArticleId");
            AddForeignKey("dbo.Comments", "ArticleId", "dbo.Articles", "Id", cascadeDelete: true);
        }
    }
}
