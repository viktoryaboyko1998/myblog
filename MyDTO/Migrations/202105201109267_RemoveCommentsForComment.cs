namespace MyDTO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveCommentsForComment : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CommentForComments", "CommentId", "dbo.Comments");
            DropForeignKey("dbo.CommentForComments", "ProfileId", "dbo.UserProfiles");
            DropIndex("dbo.CommentForComments", new[] { "ProfileId" });
            DropIndex("dbo.CommentForComments", new[] { "CommentId" });
            DropTable("dbo.CommentForComments");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CommentForComments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        DateTime = c.DateTime(nullable: false),
                        IsDelete = c.Boolean(nullable: false),
                        ProfileId = c.String(maxLength: 128),
                        CommentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.CommentForComments", "CommentId");
            CreateIndex("dbo.CommentForComments", "ProfileId");
            AddForeignKey("dbo.CommentForComments", "ProfileId", "dbo.UserProfiles", "Id");
            AddForeignKey("dbo.CommentForComments", "CommentId", "dbo.Comments", "Id", cascadeDelete: true);
        }
    }
}
