namespace MyDTO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChangeData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChangeDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        DateChange = c.DateTime(nullable: false),
                        ProfileId = c.String(maxLength: 128),
                        ArticleId = c.Int(),
                        CommentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articles", t => t.ArticleId)
                .ForeignKey("dbo.Comments", t => t.CommentId)
                .ForeignKey("dbo.UserProfiles", t => t.ProfileId)
                .Index(t => t.ProfileId)
                .Index(t => t.ArticleId)
                .Index(t => t.CommentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChangeDatas", "ProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.ChangeDatas", "CommentId", "dbo.Comments");
            DropForeignKey("dbo.ChangeDatas", "ArticleId", "dbo.Articles");
            DropIndex("dbo.ChangeDatas", new[] { "CommentId" });
            DropIndex("dbo.ChangeDatas", new[] { "ArticleId" });
            DropIndex("dbo.ChangeDatas", new[] { "ProfileId" });
            DropTable("dbo.ChangeDatas");
        }
    }
}
