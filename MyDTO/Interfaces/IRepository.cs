﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
	public interface IRepository<T> : IDisposable
	{
		void Create(T item);
		void DeleteById(int id);
		void IsDeleteById(int id);
		void Update(T item);
		Task<T> GetItemByIdAsync(string id);
		Task<IEnumerable<T>> GetAllAsync();
	}
}
