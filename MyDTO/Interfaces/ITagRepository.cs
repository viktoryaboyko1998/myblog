﻿using DAL.Entities;
using DAL.Interfaces;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public interface ITagRepository : IRepository<Tag>
	{
		Task AddToArticle(string name, int articleId);
		Task RemoveFromArticleAsync(string name, int articleId);
	}
}