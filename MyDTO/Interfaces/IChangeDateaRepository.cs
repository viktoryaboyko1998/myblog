﻿using DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IChangeDataRepository
    {
		void Create(ChangeData item);
		ChangeData GetItemByIdArticleAsync(int id);
		Task<ChangeData> GetItemByIdCommentAsync(int id);
		//Task<IEnumerable<ChangeData>> GetAllAsync();
	}
}
