﻿using System;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Identity;
using DAL.Repositories;

namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        IRepository<UserProfile> ClientManager { get; }
        ApplicationRoleManager RoleManager { get; }
        Task SaveAsync();
        IRepository<Article> Articles { get; }
        ITagRepository Tags { get; }
        IRepository<Comment> Comments { get; }
        IChangeDataRepository ChangeData { get; }
        IRepository<UserProfile> Profiles { get; }
        IRepository<Blog> Blogs { get; }
        void Save();
    }
}
