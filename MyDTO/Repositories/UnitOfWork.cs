﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Threading.Tasks;
using DAL.Identity;
using MyDTO.Repositories;

namespace DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationContext db;

        private ApplicationUserManager userManager;
        private ApplicationRoleManager roleManager;
        private IRepository<UserProfile> clientManager;

        private ArticleRepository articleRepository;
        private TagRepository tagRepository;
        private ChangeDataRepository сhangeDataRepository;
        private BlogRepository blogRepository;
        private UserProfileRepository userProfileRepository;
        private CommentRepository commentRepository;

        public UnitOfWork(string connectionString)
        {
            db = new ApplicationContext(connectionString);
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(db));
            clientManager = new UserProfileRepository(db);
        }

        public ApplicationUserManager UserManager
        {
            get { return userManager; }
        }

        public IRepository<UserProfile> ClientManager
        {
            get { return clientManager; }
        }

        public ApplicationRoleManager RoleManager
        {
            get { return roleManager; }
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    userManager.Dispose();
                    roleManager.Dispose();
                    clientManager.Dispose();
                }
                this.disposed = true;
            }
        }

        public IRepository<Article> Articles
        {
            get
            {
                if (articleRepository == null)
                    articleRepository = new ArticleRepository(db);
                return articleRepository;
            }
        }

        public IRepository<Comment> Comments
        {
            get
            {
                if (commentRepository == null)
                    commentRepository = new CommentRepository(db);
                return commentRepository;
            }
        }

        public IRepository<UserProfile> Profiles
        {
            get
            {
                if (userProfileRepository == null)
                    userProfileRepository = new UserProfileRepository(db);
                return userProfileRepository;
            }
        }

        public IRepository<Blog> Blogs
        {
            get
            {
                if (blogRepository == null)
                    blogRepository = new BlogRepository(db);
                return blogRepository;
            }
        }

        public ITagRepository Tags
        {
            get
            {
                if (tagRepository == null)
                    tagRepository = new TagRepository(db);
                return (ITagRepository)tagRepository;
            }
        }

        public IChangeDataRepository ChangeData
        {
            get
            {
                if (сhangeDataRepository == null)
                    сhangeDataRepository = new ChangeDataRepository(db);
                return сhangeDataRepository;
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }
    }
}
