﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ChangeDataRepository : IChangeDataRepository
    {
        private ApplicationContext db;

        public ChangeDataRepository(ApplicationContext context)
        {
            this.db = context;
        }

        public void Create(ChangeData item)
        {
            item.UserProfile = db.UserProfile.Find(item.ProfileId);
            item.Article = db.Articles.Find(item.ArticleId);
            item.Comment = db.Comments.Find(item.CommentId);
            db.ChangeData.Add(item);
        }

        public ChangeData GetItemByIdArticleAsync(int id)
        {
            var data = db.ChangeData.OrderByDescending(s => s.Id).Where(s => s.ArticleId == id).LastOrDefault();
            return data;
        }

        public async Task<ChangeData> GetItemByIdCommentAsync(int id)
        {
            return await db.ChangeData
             .Include(s => s.UserProfile)
             .Include(s => s.Article)
             .Include(s => s.Comment)
             .OrderByDescending(s => s.Id)
             .FirstAsync(s => s.CommentId == id);
        }
    }
}
