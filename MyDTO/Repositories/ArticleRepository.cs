﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.EF;
using DAL.Interfaces;
using DAL.Entities;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ArticleRepository : IRepository<Article>
    {
        private ApplicationContext db;

        public ArticleRepository(ApplicationContext context)
        {
            this.db = context;
        }

        public async Task<IEnumerable<Article>> GetAllAsync()
        {
            return await db.Articles
                .Include(s => s.Comments)
                .Include(s => s.Tags)
                .Include(s => s.Blog)
                .Include(s => s.UserProfile)
                .ToListAsync();
        }

        public async Task<Article> GetItemByIdAsync(string id)
        {
            var id1 = int.Parse(id);
            return await db.Articles.Include(s => s.Comments.Select(f => f.UserProfile))
                .Include(s => s.Tags)
                .Include(s => s.Blog)
                .Include(s => s.UserProfile)
                .FirstAsync(s => s.Id == id1);
        }

        public void Create(Article item)
        {
            item.UserProfile = db.UserProfile.Find(item.ProfileId);
            item.Blog = db.Blogs.Find(item.BlogId);
            var currentTags = db.Tags;
            var newTags = new List<Tag>();
            foreach (var tag in item.Tags)
            {
                if (!currentTags.Any(s => s.Title.ToLower() == tag.Title.ToLower()))
                {
                    //db.Tags.Add(tag);
                    newTags.Add(tag);
                }
                else
                {
                    newTags.Add(currentTags.First(s => s.Title.ToLower() == tag.Title.ToLower()));
                }
            }
            item.Tags = newTags;
            db.Articles.Add(item);
        }

        public  void Update(Article item)
         {
            var blog = db.Blogs.Find(item.BlogId);
            var result = db.Articles.Find(item.Id);
            result.IsDelete = item.IsDelete;
            result.Title = item.Title;
           // result.Blog = blog;
            //result.UserProfile = item.UserProfile;
            result.BlogId = item.BlogId;
            //result.Comments = item.Comments;
            result.IsAvatar = item.IsAvatar;
           // result.Tags = item.Tags;
            result.Text = item.Text;
            //db.Entry(item).State = EntityState.Modified;
        }

       /* public IEnumerable<Article> Find(Func<Article, Boolean> predicate)
        {
            return db.Articles.Where(predicate).ToList();
        }*/

        public void DeleteById(int id)
        {
            Article article = db.Articles.Find(id);
            if (article != null)
                db.Articles.Remove(article);
        }


        public void Dispose()
        {
            db.Dispose();
        }

        public void IsDeleteById(int id)
        {
            var result = db.Articles.Find(id);
            result.IsDelete = true;
        }
    }
}

