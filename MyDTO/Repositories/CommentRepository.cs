﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace MyDTO.Repositories
{
    public class CommentRepository : IRepository<Comment>
    {
        private ApplicationContext db;

        public CommentRepository(ApplicationContext context)
        {
            this.db = context;
        }

        public async Task<IEnumerable<Comment>> GetAllAsync()
        {
            return await db.Comments.Include(s => s.Article).Include(s => s.UserProfile).Include(s => s.CommentsReply).ToListAsync();
        }

        public async Task<Comment> GetItemByIdAsync(string id)
        {
            var id1 = int.Parse(id);
            return await db.Comments.FindAsync(id1);
        }

        public void Create(Comment item)
        {
            item.UserProfile = db.UserProfile.Find(item.ProfileId);
            db.Comments.Add(item);
        }

        public void Update(Comment item)
        {
            var result = db.Comments.Find(item.Id);
            result.Text = item.Text;
            //result.DateTime = item.DateTime;
        }

        public IEnumerable<Comment> Find(Func<Comment, Boolean> predicate)
        {
            return db.Comments.Where(predicate).ToList();
        }

        public void DeleteById(int id)
        {
            Comment comment = db.Comments.Find(id);
            if (comment != null)
                db.Comments.Remove(comment);
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public void IsDeleteById(int id)
        {
            throw new NotImplementedException();
        }
    }
}


