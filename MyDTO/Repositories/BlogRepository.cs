﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace MyDTO.Repositories
{
    public class BlogRepository : IRepository<Blog>
    {
        private ApplicationContext db;

        public BlogRepository(ApplicationContext context)
        {
            this.db = context;
        }

        public async Task<IEnumerable<Blog>> GetAllAsync()
        {
            return await db.Blogs
                .Include(s => s.Articles)
                .Include(s => s.UserProfile)
                .ToListAsync();
        }

        /*public Blog GetItemById(string id)
        {
            var id1 = int.Parse(id);
            return db.Blogs.Find(id1);
        }*/

        public void Create(Blog item)
        {
            var result = db.UserProfile.Find(item.ProfileId);
            item.UserProfile = result;
            db.Blogs.Add(item);
        }

        public void Update(Blog item)
        {
            //db.Entry(item).State = EntityState.Modified;
            var result = db.Blogs.Find(item.Id);
            result.IsDelete = item.IsDelete;
            result.Title = item.Title;
        }

        public IEnumerable<Blog> Find(Func<Blog, Boolean> predicate)
        {
            return db.Blogs.Where(predicate).ToList();
        }

        public void DeleteById(int id)
        {
            Blog blog = db.Blogs.Find(id);
            if (blog != null)
                db.Blogs.Remove(blog);
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public async Task<Blog> GetItemByIdAsync(string id)
        {
            var id1 = int.Parse(id);
            return await db.Blogs.Include(s => s.Articles).Include(s => s.UserProfile).FirstAsync(s => s.Id == id1);
        }

        public void IsDeleteById(int id)
        {
            var result = db.Blogs.Find(id);
            result.IsDelete = true;
        }
    }
}

