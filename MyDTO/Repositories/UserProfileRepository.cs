﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.EF;
using DAL.Interfaces;
using DAL.Entities;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UserProfileRepository : IRepository<UserProfile>
    {
        private ApplicationContext db;

        public UserProfileRepository(ApplicationContext context)
        {
            this.db = context;
        }

        public async Task<IEnumerable<UserProfile>> GetAllAsync()
        {
            return await db.UserProfile.Include(s => s.Articles).Include(s => s.Comments).Include(s => s.ApplicationUser).ToListAsync();
        }

       /* public UserProfile GetItemById(string id)
        {
            return db.UserProfile.Find(id);
        }*/

        public void Create(UserProfile item)
        {
            db.UserProfile.Add(item);
        }

        public void Update(UserProfile item)
        {
            var result = db.UserProfile.Find(item.Id);
            result.Name = item.Name;
            result.Avatar = item.Avatar;
           // result.BirthdayDate = item.BirthdayDate;
            
        }


        public IEnumerable<UserProfile> Find(Func<UserProfile, Boolean> predicate)
        {
            return db.UserProfile.Where(predicate).ToList();
        }

        public void DeleteById(int id)
        {
            UserProfile userProfile = db.UserProfile.Find(id);
            if (userProfile != null)
                db.UserProfile.Remove(userProfile);
        }


        public void Dispose()
        {
            db.Dispose();
        }

        public async Task<UserProfile> GetItemByIdAsync(string id)
        {
            return await db.UserProfile.Include(s => s.Articles).Include(s => s.Comments).FirstAsync(s => s.Id == id);
        }

        public void IsDeleteById(int id)
        {
            throw new NotImplementedException();
        }
    }
}

