﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.EF;
using DAL.Interfaces;
using DAL.Entities;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class TagRepository : ITagRepository
    {
        private ApplicationContext db;

        public TagRepository(ApplicationContext context)
        {
            this.db = context;
        }

        public async Task AddToArticle(string title, int articleId)
        {
            var result = await db.Articles.FirstAsync(s => s.Id == articleId);
            var tag = await db.Tags.FirstOrDefaultAsync(s => s.Title.ToLower() == title.ToLower());
            if (result != null)
            {
                if (tag is null)
                {
                    var item = new Tag { Title = title };
                    //db.Tags.Add(item);
                    result.Tags.Add(item);
                }
                else
                {
                    result.Tags.Add(tag);
                }
            }
        }

        public async Task RemoveFromArticleAsync(string title, int articleId)
        {
            var result = await db.Articles.Include(s => s.Tags).FirstOrDefaultAsync(s => s.Id == articleId);
            if (result != null)
            {
                var tag = result.Tags.First(s => s.Title == title);
                result.Tags.Remove(tag);
                //await db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Tag>> GetAllAsync()
        {
            return await db.Tags.ToListAsync();
        }

        public async Task<Tag> GetItemByIdAsync(string id)
        {
            var id1 = Int32.Parse(id);
            return await db.Tags.Include(s => s.Articles).FirstAsync(s => s.Id == id1);
        }

        public void Create(Tag item)
        {
            db.Tags.Add(item);
        }

        public async void Update(Tag item)
        {
            var result = await db.Tags.FindAsync(item.Id);
			if (result != null)
			{
				result.Title = item.Title;
			}
        }

        /*public IEnumerable<Tag> Find(Func<Tag, Boolean> predicate)
        {
            return db.Tags.Where(predicate).ToList();
        }*/

        public void DeleteById(int id)
        {
            Tag tag = db.Tags.Find(id);
            if (tag != null)
                db.Tags.Remove(tag);
        }


        public void Dispose()
        {
            db.Dispose();
        }

        public void IsDeleteById(int id)
        {
            throw new NotImplementedException();
        }
    }
}

