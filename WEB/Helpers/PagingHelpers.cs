﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WEB.Pagination;
//.............................

namespace WEB.Helpers
{
    public static class PagingHelpers
    {
        /*public static MvcHtmlString PageLinks(this HtmlHelper html, PageInfo pageInfo, Func<int, string> pageUrl)
        {
            TagBuilder ul = new TagBuilder("ul");
            ul.AddCssClass("pagination");
            for (int i = 1; i <= pageInfo.TotalPages; i++)
            {
                TagBuilder li = new TagBuilder("li");
                TagBuilder a = new TagBuilder("a");
                a.MergeAttribute("href", pageUrl(i));
                a.InnerHtml = i.ToString();

               
                if (i == pageInfo.PageNumber)
                {
                    li.AddCssClass("current");
                }
                li.InnerHtml += a.ToString();
                ul.InnerHtml += li.ToString();
            }
            return new MvcHtmlString(ul.ToString());
        }*/

        public static MvcHtmlString PageLinks(this HtmlHelper html, PageInfo pageInfo, Func<int, string> pageUrl)
        {
            TagBuilder ul = new TagBuilder("ul");
            ul.AddCssClass("pagination");
            if (pageInfo.TotalPages < 6)
            {
                for (int i = 1; i <= pageInfo.TotalPages; i++)
                {
                    TagBuilder li = new TagBuilder("li");
                    TagBuilder a = new TagBuilder("a");
                    a.MergeAttribute("href", pageUrl(i));
                    a.InnerHtml = i.ToString();


                    if (i == pageInfo.PageNumber)
                    {
                        li.AddCssClass("current");
                    }
                    li.InnerHtml += a.ToString();
                    ul.InnerHtml += li.ToString();
                }
            }
            else
            {
                int[] numberPage = new int[7];
                if (pageInfo.PageNumber==1)
                {
                    numberPage[0] = pageInfo.PageNumber;
                    numberPage[1] = pageInfo.PageNumber+1;
                    numberPage[2] = pageInfo.PageNumber+2;
                    numberPage[3] = 0;
                    numberPage[4] = pageInfo.TotalPages - 1;
                    numberPage[5] = pageInfo.TotalPages ;
                    numberPage[6] = -1 ;
                }

                else if (pageInfo.PageNumber == 2)
                {
                    numberPage[0] = 1;
                    numberPage[1] = 2;
                    numberPage[2] = 3;
                    numberPage[3] = 0;
                    numberPage[4] = pageInfo.TotalPages - 1;
                    numberPage[5] = pageInfo.TotalPages;
                    numberPage[6] = -1;
                }
                else if (pageInfo.PageNumber == pageInfo.TotalPages)
                {
                    numberPage[0] = 1;
                    numberPage[2] = 0;
                    numberPage[2] = pageInfo.TotalPages - 2;
                    numberPage[3] = pageInfo.TotalPages - 1;
                    numberPage[4] = pageInfo.TotalPages;
                    numberPage[5] = -1;
                    numberPage[6] = -1;
                }
                else if (pageInfo.PageNumber == pageInfo.TotalPages-1)
                {
                    numberPage[0] = 1;
                    numberPage[1] = 0;
                    numberPage[2] = pageInfo.PageNumber - 1;
                    numberPage[3] = pageInfo.PageNumber;
                    numberPage[4] = pageInfo.TotalPages;
                    numberPage[5] = -1;
                    numberPage[6] = -1;
                }


                else
                {
                    numberPage[0] = 1;
                    numberPage[1] = 0;
                    numberPage[2] = pageInfo.PageNumber-1;
                    numberPage[3] = pageInfo.PageNumber ;
                    numberPage[4] = pageInfo.PageNumber +1;
                    numberPage[5] = 0;
                    numberPage[6] = pageInfo.TotalPages;
                }
                foreach(var i in numberPage)
                {
                    if (i > 0)
                    {
                        TagBuilder li = new TagBuilder("li");
                        TagBuilder a = new TagBuilder("a");
                        a.MergeAttribute("href", pageUrl(i));
                        a.InnerHtml = i.ToString();


                        if (i == pageInfo.PageNumber)
                        {
                            li.AddCssClass("current");
                        }
                        li.InnerHtml += a.ToString();
                        ul.InnerHtml += li.ToString();
                    }
                    else if (i==0)
                    {
                        TagBuilder li = new TagBuilder("li");
                        li.AddCssClass("li-triple-dot");
                        TagBuilder p = new TagBuilder("p");
                        p.AddCssClass("triple-dot");
                        p.InnerHtml = "...";
                       
                        li.InnerHtml += p.ToString();
                        ul.InnerHtml += li.ToString();
                    }
                }
            }
            return new MvcHtmlString(ul.ToString());
        }
    }
}