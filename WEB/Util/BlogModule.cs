﻿using Ninject.Modules;
using BLL.Services;
using BLL.Interfaces;
using BLL.DTO;
using System.Web.Mvc;
using AutoMapper;
using WEB.Mappers;

namespace WEB.Util
{
    public class BlogModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IArticleService>().To<ArticleService>();
            //Bind<IService<ArticleDTO>>().To<ArticleService>();
            Bind<IService<BlogDTO>>().To<BlogService>();
            //Bind<IUserService>().To<UserService>();
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile<AutomapperProfileWEB>(); cfg.AddProfile<AutomapperProfile>(); });
            Bind<IMapper>().ToConstructor(s => new Mapper(mapperConfiguration));

            Unbind<ModelValidatorProvider>();
        }
    }
}