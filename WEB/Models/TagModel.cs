﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WEB.Models;

namespace WEB.Models
{
    public class TagModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public virtual ICollection<ArticleModel> Articles { get; set; }
    }
}