﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WEB.Models
{
    public  class BlogModel
    {
		public int Id { get; set; }
		[Required(ErrorMessage = "This field is required.")]
		[MinLength(3, ErrorMessage = "Min length is 3")]
		[AllowHtml]
		public string Title { get; set; }
		[DataType(DataType.Date)]
		public DateTime DateTime { get; set; } = DateTime.Now;
		public bool IsDelete { get; set; } = false;
		public ICollection<ArticleModel> Articles { get; set; }
		public UserModel UserProfile { get; set; }
		public string ProfileId { get; set; }
	}
}