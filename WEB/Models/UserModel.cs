﻿using BLL.DTO;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WEB.Models
{
    public class UserModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        [AllowHtml]
        public string Password { get; set; }
       // public string Role { get; set; }
        //public string RoleId { get; set; }
        public string RoleName { get; set; }
        //public string UserName { get; set; }
        [AllowHtml]
        public string PasswordCheckCurrent { get; set; }

        [RegularExpression(@"^(?:[A-Z a-z0-9]{4,20})", ErrorMessage = "Only: a-z, 0-9, @, _, characters are allowed. Range: 4-20 symbols")]
        [AllowHtml]
        public string Name { get; set; }
        public string Avatar { get; set; }
        [DataType(DataType.Date)]
        public DateTime BirthdayDate { get; set; }
        public bool IsDelete { get; set; } = false;
        public IList<string> UserRoles { get; set; }
        // public ICollection<IdentityUserRole> UserRoles { get; set; }
         public List<IdentityRole> AllRoles { get; set; }
        // public List<IdentityRole> AllRoles { get; set; }

        public UserModel UserProfile { get; set; }
        public BlogDTO Blog { get; set; }

       

        public ICollection<ArticleDTO> Articles { get; set; }
        public ICollection<CommentDTO> Comments { get; set; }
        public ICollection<BlogDTO> Blogs { get; set; }
    }
}