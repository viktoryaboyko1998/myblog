﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WEB.Models
{
    public class ChangeDataModel
    {
		public int Id { get; set; }
		public string Description { get; set; }
		public DateTime DateChange { get; set; }


		[ForeignKey("UserProfile")]
		public string ProfileId { get; set; }
		public UserModel UserProfile { get; set; }


		[ForeignKey("Article")]
		public int? ArticleId { get; set; }
		public ArticleModel Article { get; set; }
		[ForeignKey("Comment")]
		public int? CommentId { get; set; }
		public CommentModel Comment { get; set; }
	}
}