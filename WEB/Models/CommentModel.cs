﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WEB.Models
{
    public class CommentModel
    {
		public int Id { get; set; }
		[Required(ErrorMessage = "This field is required.")]
		[DataType(DataType.MultilineText)]
		[AllowHtml]
		public string Text { get; set; }
		[DataType(DataType.Date)]
		public DateTime DateTime { get; set; } = DateTime.Now;
		public bool IsDelete { get; set; } = false;
		//public bool IsChange { get; set; } = false;
		//public DateTime DataChange { get; set; } = DateTime.Now;
		public  UserModel UserProfile { get; set; }
		public  ArticleModel Article { get; set; }
		public int? ArticleId { get; set; }
		public int? CommentsReplyId { get; set; }
		public ICollection<CommentModel> CommentsReply { get; set; }
		public string ProfileId { get; set; }
	}
}