﻿using BLL.DTO;
using WEB.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WEB.Models
{
    public class ArticleModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [MinLength(3, ErrorMessage = "Min length is 3")]
        [AllowHtml]
        public string Title { get; set; }
        public bool IsAvatar { get; set; } = false;
        [DataType(DataType.Date)]
        public DateTime DateTime { get; set; } = DateTime.Now;
        [Required(ErrorMessage = "This field is required.")]
        [MinLength(10, ErrorMessage = "Min Length is 10")]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Text { get; set; }

        public int BlogId { get; set; }
        public BlogModel Blog { get; set; }
        public string ProfileId { get; set; }
        public UserModel UserProfile { get; set; }
        public Dictionary<string, bool> TagsName { get; set; }
        public ICollection<CommentDTO> Comments { get; set; }
        public ICollection<TagDTO> Tags { get; set; }

        
    }
}