﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WEB.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "This field is required.")]
        [EmailAddress(ErrorMessage = "Input correct email address.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [AllowHtml]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
