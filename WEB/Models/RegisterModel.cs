﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WEB.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "This field is required.")]
        [EmailAddress(ErrorMessage = "Input correct email address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [MinLength(6, ErrorMessage = "Password should be at least 6 symbols in length.")]
        [DataType(DataType.Password)]
        [AllowHtml]
        public string Password { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Passwords is not equals.")]
        [AllowHtml]
        public string ConfirmPassword { get; set; }
        [DataType(DataType.Date)]
        public DateTime BirthdayDate { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [RegularExpression(@"^(?:[A-Z a-z0-9]{4,20})", ErrorMessage = "Only: a-z, 0-9, @, _, characters are allowed. Range: 4-20 symbols")]
        [AllowHtml]
        public string Name { get; set; }
       // public string Avatar { get; set; }
    }
}
