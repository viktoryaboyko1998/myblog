using Ninject;
using Ninject.Web.Mvc;
using Ninject.Modules;
using BLL.Infrastructure;
using WEB.Util;

using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace WEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // внедрение зависимостей
            NinjectModule orderModule = new BlogModule();
            NinjectModule serviceModule = new ServiceModule("MyBlogConnection");
            var kernel = new StandardKernel(orderModule, serviceModule);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }
    }
}
