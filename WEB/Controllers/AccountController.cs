﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using BLL.DTO;
using System.Security.Claims;
using BLL.Interfaces;
using BLL.Infrastructure;
using WEB.Models;
using AutoMapper;
using System.Net;
using Microsoft.AspNet.Identity;
using System.Text;
using System;
using WEB.Pagination;
using System.Linq;
using System.IO;

namespace Controllers
{
    /// <summary>
    /// Class that provide all functionality related to User.
    /// </summary>
    public class AccountController : Controller
    {
        private readonly IMapper mapper;

        public AccountController(IMapper map)
        {
            mapper = map;
        }

        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        /// <summary>
        /// Show information about exact User.
        /// </summary>
        /// <param name="id">Id of User.</param>
        /// <returns>View with information about exact User.</returns>
        public async Task<ActionResult> Info(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserDTO userDTO = await UserService.GetItemByIdAsync(id);
            var user = mapper.Map<UserDTO, UserModel>(userDTO);
            if (userDTO == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        /// <summary>
        /// Show information about all Users.
        /// </summary>
        /// <param name="page">Number page of users for pagination.</param>
        /// <returns>View with information about all Users.</returns>
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> UsersAdmin(int page = 1)
        {
            var userDTO = await UserService.GetAllAsync();
            var user = mapper.Map<IEnumerable<UserDTO>, List<UserModel>>(userDTO);

            int pageSize = 5; // количество объектов на страницу
            IEnumerable<UserModel> usersPerPages = user.Skip((page - 1) * pageSize).Take(pageSize);
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = user.Count };
            IndexViewModel<UserModel> ivm = new IndexViewModel<UserModel> { PageInfo = pageInfo, Models = usersPerPages };
            return View("UsersAdmin", ivm);

        }


        public async Task<ActionResult> CreateAdmin()
         {
             UserDTO userDto = new UserDTO
             {
                 Email = "admin@gmail.com",
                 Password = "111111",
                 BirthdayDate = DateTime.Now,
                 Name = "Adminnn",
                 RoleName = "admin"
             };
             OperationDetails operationDetails = await UserService.Create(userDto);
             if (operationDetails.Succedeed)
                 return View("SuccessRegister");
             else
                 ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
             return View(userDto);
         }
        


        /// <summary>
        /// Show login form.
        /// </summary>
        /// <returns>View with login form.</returns>
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// Receives data about the user using the post method and login the user in system
        /// </summary>
        /// <param name="model">Form with user data.</param>
        /// <returns>Redirect to Articles, or return login form with errors.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO { Email = model.Email, Password = model.Password };
                ClaimsIdentity claim = await UserService.Authenticate(userDto);
                if (claim == null)
                {
                    ModelState.AddModelError("", "Wrong login or password.");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    return RedirectToAction("Index", "Article");
                }
            }
            return View(model);
        }


        /// <summary>
        /// Logout current user.
        /// </summary>
        /// <returns>Redirect to Articles</returns>
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Article");
        }

        /// <summary>
        /// Show register form.
        /// </summary>
        /// <returns>View with register form.</returns>
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// Receives data about the user using the post method and register the user in system
        /// </summary>
        /// <param name="model">Form with user data.</param>
        /// <returns>Redirect to View with success mesage, or return login form with errors.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.BirthdayDate>DateTime.Now)
                {
                    ModelState.AddModelError("BirthdayDate", "Enter correct date");
                }
                UserDTO userDto = new UserDTO
                {
                    Email = model.Email,
                    Password = model.Password,
                    BirthdayDate = model.BirthdayDate,
                    Name = model.Name,
                    RoleName = "user"
                };
                OperationDetails operationDetails = await UserService.Create(userDto);
                if (operationDetails.Succedeed)
                    return View("SuccessRegister");
                else
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
            }
            return View(model);
        }



        //change user role
        /* [HttpPost]
         public async Task<ActionResult> Edit(string userId, List<string> roles)
         {
             // получаем пользователя
             UserDTO user = await UserService.UpdateItemAsyncRoles(userId);
             if (user != null)
             {
                 await UserService.UpdateItemAsync(user, roles);
                 // получем список ролей пользователя


                 return RedirectToAction("Info", new { id =  userId});
             }
             return HttpNotFound();
         }*/

        /// <summary>
        /// Show edit form.
        /// </summary>
        /// <param name="id">Id of User.</param>
        /// <returns>View with edit form, or return  form with errors.</returns>
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserDTO userDTO = await UserService.GetItemByIdAsync(id);
            if (userDTO == null)
            {
                return HttpNotFound();
            }
            if ((userDTO.Id == User.Identity.GetUserId())|| (User.IsInRole("admin")))
            {
                var user = mapper.Map<UserDTO, UserModel>(userDTO);           
                return View(user);  
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Receives data about the user using the post method and edit user`s data in system
        /// </summary>
        /// <param name="userModel">Form with user data.</param>
        /// <param name="uploadImage">Upload image -user avatar.</param>
        /// <returns>Redirect to View with success mesage, or return login form with errors.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(UserModel userModel, HttpPostedFileBase uploadImage)
        {
            if (ModelState.IsValid)
            {
                var userDTO = mapper.Map<UserModel, UserDTO>(userModel);
                var id = userModel.Id;
                if (uploadImage != null)
                {
                    var fileName = Path.GetFileName(id + ".jpg");
                    var path = Path.Combine(Server.MapPath("~/Content/img/users/"), fileName );
                    uploadImage.SaveAs(path);
                    userDTO.Avatar = fileName;
                }
                var result = await UserService.UpdateItemAsync(userDTO);
                if (!result.GetEnumerator().MoveNext())
                    return RedirectToAction("Info/", new { id = userModel.Id });
                else
                {
                    StringBuilder error = new StringBuilder();
                    foreach (var er in result)
                    {
                        error = error.Append(" " + er);
                    }
                    ViewBag.ErrorPassword = error;
                    return RedirectToAction("Info/", new { id = userModel.Id });             
                }
            }
            return View(userModel);
        }
    }
}