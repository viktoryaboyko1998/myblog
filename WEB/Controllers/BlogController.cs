﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using Microsoft.AspNet.Identity;
using WEB.Models;
using WEB.Pagination;

namespace WEB.Controllers
{
    public class BlogController : Controller
    {
        private readonly IService<BlogDTO> blogService;
        private readonly IMapper mapper;

        public BlogController(IService<BlogDTO> servB,  IMapper map)
        {
            blogService = servB;
            mapper = map;
        }

        /// <summary>
        /// Show information about all blogs.
        /// <param name="searchTitle">Specifies what text should be included in the title of Blogs.</param>
        /// <param name="sorting">Indicates on what basis to sort.</param>
        /// <param name="page">Number page of blogs for pagination</param>
        /// <returns>View with blogs.</returns>
        public async Task<ActionResult> Index(string id, string searchTitle,  Sort? sorting, int page = 1)
        {
            ViewBag.searchTitle = searchTitle;
            ViewBag.sorting = sorting;
            IEnumerable<BlogDTO> blogDTO;

            if (id is null)
            {
                blogDTO = await blogService.GetAllAsync(searchTitle, null, null, sorting);
               
            }
            else
            {
                blogDTO = await blogService.GetAllByUserIdIsDeleteAsync(id, searchTitle, null, null, sorting);
            }
            var blog = mapper.Map<IEnumerable<BlogDTO>, List<BlogModel>>(blogDTO);
            int pageSize = 5; // количество объектов на страницу
            IEnumerable<BlogModel> blogsPerPages = blog.Skip((page - 1) * pageSize).Take(pageSize);
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = blog.Count };
            IndexViewModel<BlogModel> ivm = new IndexViewModel<BlogModel> { PageInfo = pageInfo, Models = blogsPerPages };
            return View(ivm);
        }

        /// <summary>
        /// Show information about all blogs.
        /// <param name="searchTitle">Specifies what text should be included in the title of Blogs.</param>
        /// <param name="sorting">Indicates on what basis to sort.</param>
        /// <param name="page">Number page of blogs for pagination</param>
        /// <returns>View with blogs.</returns>
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> BlogsAdmin(string id, string searchTitle,  Sort? sorting, int page = 1)
        {
            ViewBag.searchTitle = searchTitle;
            ViewBag.sorting = sorting;
            IEnumerable<BlogDTO> blogDTO;
            blogDTO = await blogService.GetAllAsync(searchTitle,null,null,  sorting);

            var blog = mapper.Map<IEnumerable<BlogDTO>, List<BlogModel>>(blogDTO);

            int pageSize = 5; // количество объектов на страницу
            IEnumerable<BlogModel> blogsPerPages = blog.Skip((page - 1) * pageSize).Take(pageSize);
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = blog.Count };
            IndexViewModel<BlogModel> ivm = new IndexViewModel<BlogModel> { PageInfo = pageInfo, Models = blogsPerPages };
            return View("BlogsAdmin", ivm);
        }


        /// <summary>
        /// Show information about exact Blog and its articles.
        /// </summary>
        /// <param name="id">Id of Blog.</param>
        /// <param name="page">Number page of articles for pagination</param>
        /// <returns>View with information about exact blog and its articles.</returns>
        public async Task<ActionResult> Details(int? id, int page=1)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogDTO blogDTO = await blogService.GetItemAsync(id);
            if (blogDTO == null)
            {
                return HttpNotFound();
            }
            var blog = mapper.Map<BlogDTO, BlogModel>(blogDTO);
            var articles = blog.Articles;
            if (articles.Count() == 0)
            {
                articles = new List<ArticleModel> { new ArticleModel { BlogId = blog.Id, Blog = blog, UserProfile = blog.UserProfile, ProfileId = blog.ProfileId } };
                ViewBag.CountArticle = 0;
            }

            int pageSize = 5; // количество объектов на страницу
            IEnumerable<ArticleModel> articlePerPages = articles.Skip((page - 1) * pageSize).Take(pageSize);
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = articles.Count };
            IndexViewModel<ArticleModel> ivm = new IndexViewModel<ArticleModel> { PageInfo = pageInfo, Models = articlePerPages };
            return View(ivm);
        }

        /// <summary>
        /// Show create form.
        /// </summary>
        /// <returns>View with create form.</returns>
        public async Task<ActionResult> Create()
        {
            return View();
        }

        /// <summary>
        /// Receives data about the blog using the post method and create the blog in system
        /// </summary>
        /// <param name="blogModel">Form with blog data.</param>
        /// <returns>Redirect to Action Index .</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,IsDelete")] BlogModel blogModel)
        {
            if (ModelState.IsValid)
            {
                blogModel.DateTime = DateTime.Now;
                var usId = User.Identity.GetUserId();
                blogModel.ProfileId = usId;
                var blog = mapper.Map<BlogModel, BlogDTO>(blogModel);
                await blogService.AddItemAsync(blog);
                return RedirectToAction("Index", new { id = usId });
            }

            return View(blogModel);
        }

        /// <summary>
        /// Show edit form.
        /// </summary>
        /// <param name="id">Id of blog.</param>
        /// <returns>View with edit form , or return  form with errors.</returns>
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogDTO blogDTO = await blogService.GetItemAsync(id);
            if (blogDTO == null)
            {
                return HttpNotFound();
            }
            if (blogDTO.ProfileId == User.Identity.GetUserId() || User.IsInRole("admin"))
            {
                var blog = mapper.Map<BlogDTO, BlogModel>(blogDTO);            
                return View(blog);
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            
        }

        /// <summary>
        /// Receives data about the blog using the post method and edit blog`s data in system
        /// </summary>
        /// <param name="blogModel">Form with blog data.</param>
        /// <returns>Redirect to action Index.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,IsDelete")] BlogModel blogModel)
        {
            if (ModelState.IsValid)
            {
                var blog = mapper.Map<BlogModel, BlogDTO>(blogModel);
                await blogService.UpdateItemAsync(blog);
                return RedirectToAction("Index");
            }
            return View(blogModel);
        }

        /// <summary>
        /// Show form with confirm delete.
        /// </summary>
        /// <param name="id">Id of blog.</param>
        /// <returns>View with blog`s date.</returns>
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            BlogDTO blogDTO = await blogService.GetItemAsync(id);
            if (blogDTO == null)
            {
                return HttpNotFound();
            }
            if (blogDTO.ProfileId == User.Identity.GetUserId() || User.IsInRole("admin"))
            {
                var blog = mapper.Map<BlogDTO, BlogModel>(blogDTO);
                return View(blog);              
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Delete blog in system
        /// </summary>
        /// <param name="id">Id of blog.</param>
        /// <returns>Redirect to action Index.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await blogService.DeleteItemByIIsDeletedAsync(id);     
            return RedirectToAction("Index");
        }
    }
}
