﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WEB.Mappers;
using WEB.Models;
using WEB.Pagination;

namespace WEB.Controllers
{
    public class ArticleController : Controller
    {
        readonly IArticleService articleService;
        private readonly IMapper mapper;

        public ArticleController(IArticleService serv, IMapper map)
        {
            articleService = serv;
            mapper = map;
        }

        /// <summary>
        /// Show information about all Article.
		/// <param name="searchTitle">Specifies what text should be included in the title of Articles.</param>
		/// <param name="searchText">Specifies what text should be included in the text of articles</param>
		/// <param name="tags">Indicates which tag the article should have.</param>	
        /// <param name="sorting">Indicates on what basis to sort.</param>
		/// <param name="page">Number page of articles for pagination</param>
		/// <returns>View with articles.</returns>
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> ArticlesAdmin(string searchTitle, string searchText, string tags, Sort? sorting, int page = 1)
        {
            ViewBag.searchTitle = searchTitle;
            ViewBag.searchText = searchText;
            ViewBag.sorting = sorting;
            if (tags == "System.Collections.Generic.List`1[BLL.DTO.TagDTO]")
                tags = null;
            ViewBag.tag = tags;
            IEnumerable<ArticleDTO> articleDTO;
            articleDTO = await articleService.GetAllIsDeleteAsync(searchTitle, searchText, tags, sorting);

            ViewBag.Tags = await articleService.GetAllTagsAsync();
            var article = mapper.Map<IEnumerable<ArticleDTO>, List<ArticleModel>>(articleDTO);
            int pageSize = 6;
            IEnumerable<ArticleModel> articlesPerPages = article.Skip((page - 1) * pageSize).Take(pageSize);
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = article.Count };
            IndexViewModel<ArticleModel> ivm = new IndexViewModel<ArticleModel> { PageInfo = pageInfo, Models = articlesPerPages };
            return View("ArticlesAdmin", ivm);
        }

        /// <summary>
        /// Show information about all Article.
        /// <param name="searchTitle">Specifies what text should be included in the title of Articles.</param>
        /// <param name="searchText">Specifies what text should be included in the text of articles</param>
        /// <param name="tags">Indicates which tag the article should have.</param>	
        /// <param name="sorting">Indicates on what basis to sort.</param>
        /// <param name="page">Number page of articles for pagination</param>
        /// <returns>View with articles.</returns>
        public async Task<ActionResult> Index(string id, string searchTitle, string searchText, string tags, Sort? sorting, int page = 1)
        {
            ViewBag.searchTitle = searchTitle;
            ViewBag.searchText = searchText;
            ViewBag.sorting = sorting;
            if (tags == "System.Collections.Generic.List`1[BLL.DTO.TagDTO]")
                tags = null;
            ViewBag.tag = tags;
            IEnumerable<ArticleDTO> articleDTO;
            int pageSize = 3;
            if (!(id is null))
            {
                articleDTO = await articleService.GetAllByUserIdIsDeleteAsync(id, searchTitle, searchText, tags, sorting);

            }
            else
            {
                articleDTO = await articleService.GetAllIsDeleteAsync(searchTitle, searchText, tags, sorting);
            }

            ViewBag.Tags = await articleService.GetAllTagsAsync();
            var article = mapper.Map<IEnumerable<ArticleDTO>, List<ArticleModel>>(articleDTO);

            IEnumerable<ArticleModel> articlesPerPages = article.Skip((page - 1) * pageSize).Take(pageSize);
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = article.Count };
            IndexViewModel<ArticleModel> ivm = new IndexViewModel<ArticleModel> { PageInfo = pageInfo, Models = articlesPerPages };

            return View(ivm);
        }


        /// <summary>
        /// Show information about exact Article.
        /// </summary>
        /// <param name="id">Id of Article.</param>
        /// <param name="page">Number page of coments for pagination</param>
        /// <returns>View with information about exact Article and its comments.</returns>
        public async Task<ActionResult> Details(int? id, int page = 1)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArticleDTO articleDTO = await articleService.GetItemAsync(id);
            if (articleDTO == null)
            {
                return HttpNotFound();
            }
            var article = mapper.Map<ArticleDTO, ArticleModel>(articleDTO);
            ViewBag.Tags = await articleService.GetAllTagsAsync();

            //ViewBag.ChangedateArticle = await articleService.GetLastChangeArticleByIdAsync(article.Id);
            var comment = mapper.Map<IEnumerable<CommentDTO>, IEnumerable<CommentModel>>(articleDTO.Comments);
            ViewBag.CountComment = comment.Count();
            if (comment.Count() == 0)
            {
                comment = new List<CommentModel> { new CommentModel { Article = article, UserProfile = article.UserProfile, ArticleId = article.Id, ProfileId = article.ProfileId } };
                ViewBag.CountComment = 0;
            }

            int pageSize = 5;
            IEnumerable<CommentModel> comentPerPages = comment.Skip((page - 1) * pageSize).Take(pageSize);
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = comment.Count() };
            IndexViewModel<CommentModel> ivm = new IndexViewModel<CommentModel> { PageInfo = pageInfo, Models = comentPerPages };
            return View(ivm);
        }

        /// <summary>
        /// Show create form.
        /// </summary>
        /// <returns>View with create form.</returns>
        public async Task<ActionResult> Create()
        {
            ViewBag.Blogs = await GetBlogsByUser(User.Identity.GetUserId());

            var allTags = await articleService.GetAllTagsAsync();
            var article = new ArticleModel();
            article.TagsName = new Dictionary<string, bool>();
            allTags.ToList().ForEach(s => article.TagsName.Add(s.Title, false));

            return View(article);
        }

        /// <summary>
        /// Receives data about the article using the post method and create the article in system
        /// </summary>
        /// <param name="articleModel">Form with article data.</param>
        /// <param name="uploadImage">Upload image - main image for article..</param>
        /// <returns>Redirect to Action Index .</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,IsDelete, IsAvatar, BlogId, Text, TagsName")] ArticleModel articleModel, HttpPostedFileBase uploadImage)
        {
            if (ModelState.IsValid)
            {
                List<TagDTO> tags = new List<TagDTO>();
                foreach (var tag in articleModel.TagsName)
                {
                    if (tag.Value)
                    {
                        tags.Add(new TagDTO { Title = tag.Key });
                    }
                }
                articleModel.Tags = tags;
                articleModel.TagsName = null;
                articleModel.DateTime = DateTime.Now;
                var usId = User.Identity.GetUserId();
                articleModel.ProfileId = usId;

                var article = mapper.Map<ArticleModel, ArticleDTO>(articleModel);
                if (uploadImage != null)
                    article.IsAvatar = true;
                await articleService.AddItemAsync(article);
                if (uploadImage != null)
                {
                    var allArtcleByUser = await articleService.GetAllByUserIdAsync(usId, null, null, null, null);
                    int aId  = allArtcleByUser.First().Id ;
                    var fileName = Path.GetFileName(usId + "_" + aId + ".jpg");
                    var path = Path.Combine(Server.MapPath("~/Content/img/articles/"), fileName);
                    uploadImage.SaveAs(path);          
                }

                return RedirectToAction("Index");
            }

            return View(articleModel);
        }

        /// <summary>
        /// Show edit form.
        /// </summary>
        /// <param name="id">Id of Article.</param>
        /// <returns>View with edit form , or return  form with errors.</returns>
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArticleDTO articleDTO = await articleService.GetItemAsync(id);
            if (articleDTO == null)
            {
                return HttpNotFound();
            }
            if (articleDTO.ProfileId == User.Identity.GetUserId() || User.IsInRole("admin"))
            {
                var article = mapper.Map<ArticleDTO, ArticleModel>(articleDTO);
                var allTags = await articleService.GetAllTagsAsync();
                article.TagsName = new Dictionary<string, bool>();
                var isTag = false;
                foreach (var item in allTags)
                {
                    isTag = false;
                    foreach (var i in article.Tags)
                    {
                        if (item.Title == i.Title)
                            isTag = true;
                    }
                    if (!isTag)
                    {
                        article.TagsName.Add(item.Title, false);
                    }
                    else
                        article.TagsName.Add(item.Title, true);
                }
                if (User.IsInRole("admin"))
                {
                    ViewBag.Blogs = await GetBlogsByUser(article.ProfileId);
                }
                else
                {
                    ViewBag.Blogs = await GetBlogsByUser(User.Identity.GetUserId());
                }
                return View(article);

            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Receives data about the article using the post method and edit article`s data in system
        /// </summary>
        /// <param name="articleModel">Form with article data.</param>
        /// <param name="uploadImage">Upload image - main article`s image.</param>
        /// <returns>Redirect to action Index.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,IsDelete, IsAvatar, BlogId, Text, Text, TagsName, ProfileId")] ArticleModel articleModel, HttpPostedFileBase uploadImage)
        {
            if (ModelState.IsValid)
            {
                List<TagDTO> tags = new List<TagDTO>();
                foreach (var tag in articleModel.TagsName)
                {
                    if (tag.Value)
                    {
                        tags.Add(new TagDTO { Title = tag.Key });
                    }
                }
                articleModel.Tags = tags;
                articleModel.TagsName = null;
                var usId = articleModel.ProfileId;
                var article = mapper.Map<ArticleModel, ArticleDTO>(articleModel);
                var oldArticle = await articleService.GetItemAsync(article.Id);
                article.IsAvatar = oldArticle.IsAvatar;
                if (uploadImage != null)
                {
                    var fileName = Path.GetFileName(usId + "_" + articleModel.Id + ".jpg");
                    var path = Path.Combine(Server.MapPath("~/Content/img/articles/"), fileName);
                    uploadImage.SaveAs(path);
                    article.IsAvatar = true;
                }
                await articleService.UpdateItemAsync(article);
                return RedirectToAction("Index");
            }
            return View(articleModel);
        }

        /// <summary>
        /// Show form with confirm delete.
        /// </summary>
        /// <param name="id">Id of article.</param>
        /// <returns>View with article`s date.</returns>
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ArticleDTO articleDTO = await articleService.GetItemAsync(id);
            if (articleDTO == null)
            {
                return HttpNotFound();
            }
            if (articleDTO.ProfileId == User.Identity.GetUserId() || User.IsInRole("admin"))
            {
                var article = mapper.Map<ArticleDTO, ArticleModel>(articleDTO);
                return View(article);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Delete article in system (IsDelete)
        /// </summary>
        /// <param name="id">Id of article.</param>
        /// <returns>Redirect to action Index.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await articleService.DeleteItemByIIsDeletedAsync(id);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Show edit form.
        /// </summary>
        /// <param name="id">Id of comment.</param>
        /// <returns>View with edit form</returns>
        public async Task<ActionResult> EditComment(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var comments = await articleService.GetAllCommentAsync();
            CommentDTO commentDTO = null;
            foreach (var comment in comments)
            {
                if (comment.Id == id)
                    commentDTO = comment;
            }
            if (commentDTO == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (commentDTO.ProfileId == User.Identity.GetUserId() || User.IsInRole("admin"))
                {
                    var commentModel = mapper.Map<CommentDTO, CommentModel>(commentDTO);
                    return View(commentModel);
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Receives data about the comment using the post method and edit comment`s data in system
        /// </summary>
        /// <param name="commentModel">Form with comment data.</param>
        /// <returns>Redirect to View Details Article.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditComment(CommentModel commentModel)
        {
            if (ModelState.IsValid)
            {
                var comentDTO = mapper.Map<CommentModel, CommentDTO>(commentModel);
                var articleId = comentDTO.ArticleId;
                if (comentDTO.CommentsReplyId != null)
                    comentDTO.ArticleId = null;
                await articleService.UpdateCommentArticleAsync(comentDTO);
                return RedirectToAction("Details/", new { id = articleId });
            }
            return View(commentModel);
        }

        /// <summary>
        /// Show form with confirm delete.
        /// </summary>
        /// <param name="id">Id of comment.</param>
        /// <returns>View with confirm.</returns>
        public async Task<ActionResult> DeleteComment(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var comments = await articleService.GetAllCommentAsync();
            CommentDTO commentDTO = null;
            foreach (var comment in comments)
            {
                if (comment.Id == id)
                    commentDTO = comment;
            }
            if (commentDTO == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (commentDTO.ProfileId == User.Identity.GetUserId() || User.IsInRole("admin"))
                {
                    var commentModel = mapper.Map<CommentDTO, CommentModel>(commentDTO);   
                    return View(commentModel);
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Delete comment in system
        /// </summary>
        /// <param name="id">Id of comment.</param>
        /// <returns>Redirect to action Index.</returns>
        [HttpPost, ActionName("DeleteComment")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmedComment(int id)
        {
            var comments = await articleService.GetAllCommentAsync();
            CommentDTO commentDTO = null;
            foreach (var comment in comments)
            {
                if (comment.Id == id)
                    commentDTO = comment;
            }
            if (commentDTO == null)
            {
                return HttpNotFound();
            }
            await articleService.DeleteCommentArticleAsync(id);
            return RedirectToAction("Details", new { id = commentDTO.ArticleId });
        }

        /// <summary>
        /// Show create form.
        /// </summary>
        /// <returns>View with create form.</returns>
        [HttpPost]
        [ActionName("Details")]
        public async Task<ActionResult> AddComment(CommentModel model)
        {

            if (ModelState.IsValid)
            {
                var id = User.Identity.GetUserId();
                model.ProfileId = id;
                model.DateTime = DateTime.Now;
                var articleId = model.ArticleId;
                if (model.CommentsReplyId != null)
                    model.ArticleId = null;
                if (Request["ArticleId"] != null)
                {
                    model.ArticleId = Int32.Parse(Request["ArticleId"]);
                   
                }
                if (Request["CommentsReplyId"] != null)
                {
                    model.CommentsReplyId = Int32.Parse(Request["CommentsReplyId"]);
                   
                }
                var article1 = await articleService.GetItemAsync(articleId);
                await articleService.AddCommentToArticleAsync(mapper.Map<CommentDTO>(model));

                
                var comment = mapper.Map<IEnumerable<CommentDTO>, IEnumerable<CommentModel>>(article1.Comments);
                ModelState.Clear();
                int pageSize = 5;
                int page = 1;
                IEnumerable<CommentModel> comentPerPages = comment.Skip((page - 1) * pageSize).Take(pageSize);
                PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = comment.Count() };
                IndexViewModel<CommentModel> ivm = new IndexViewModel<CommentModel> { PageInfo = pageInfo, Models = comentPerPages };
                return RedirectToAction("Details", new { id = articleId });
            }
            return View(model);
        }


        /// <summary>
        /// Get all blogs of User
        /// </summary>
        /// <param name="id">User`s id</param>
        /// <returns>SelectList blogs/ </returns>
        public async Task<SelectList> GetBlogsByUser(string id)
        {
            var allBlogs = await articleService.GetAllBlogsAsync();
            List<BlogDTO> blog = new List<BlogDTO>();
            foreach (var b in allBlogs)
            {
                if (b.ProfileId == id)
                    blog.Add(b);
            }
            var blogModel = mapper.Map<List<BlogDTO>, List<BlogModel>>(blog);
            SelectList blogs = new SelectList(blogModel, "Id", "Title");
            return blogs;
        }

    }
}