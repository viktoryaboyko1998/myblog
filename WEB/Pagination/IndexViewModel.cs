﻿using System.Collections.Generic;
using WEB.Models;

namespace WEB.Pagination
{
    public class IndexViewModel<T>
    {
        public IEnumerable<T> Models { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}